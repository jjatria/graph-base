package Graph::Base;
# ABSTRACT: A Graph class designed to be extended

use Moo;
use Carp 'croak';
use Class::Load qw( load_class );
use Scalar::Util qw( refaddr blessed );

with 'Graph::Base::Role::Graph';

our $VERSION = '0.001007';
use namespace::clean;

has node_class => ( is => 'ro', default => 'Graph::Base::Node' );
has edge_class => ( is => 'ro', default => 'Graph::Base::Edge' );

sub nodes { values %{ shift->{node_map} //= {} } }

sub vertices { goto \&nodes }

sub edges {
    my $self = shift;

    my %edges;
    for my $node ($self->nodes) {
        $edges{ refaddr $_ } = $_ for $node->outgoing;
    }

    return values %edges;
}

# NOTE If this is not the genus, then what is this?
sub genus {
    my $edges = $_[0]->edges;
    $edges /= 2 unless $_[0]->directed;
    return $edges - $_[0]->nodes + 1;
}

sub density {
    my $self = shift;

    my $nodes     = $self->nodes;
    my $edges     = $self->edges;
    my $max_edges = $nodes * ($nodes - 1) or return 0;

    unless ( $self->directed ) {
        $max_edges /= 2;
        $edges     /= 2;
    }

    return $edges / $max_edges;
}

sub add_node {
    my $self = shift;

    my ($item) = @_;

    my ( $node, %args );

    if ( blessed $item && $item->DOES('Graph::Base::Role::Node') ) {
        if ( $item->graph && refaddr $item->graph == refaddr $self ) {
            $node = $item;
        }
        else {
            $node = $item->clone;
            $node->graph($self);
            $node->clear_edge_class;
        }
    }
    elsif ( @_ == 1 ) {
        if ( $self->{node_map}{$item} ) {
            $node = $self->{node_map}{$item};
        }
        else {
            $args{name} = $item;
        }
    }
    else {
        %args = @_;
    }

    unless ($node) {
        load_class $self->node_class;
        $node = $self->node_class->new( graph => $self, %args );
    }

    return unless $node;

    my $id  = $node->id;
    my $map = $self->{node_map} //= {};

    return $map->{$id} if $map->{$id};
    return $map->{$id} = $node;
}

sub has_node {
    my ( $self, $node ) = @_;
    return unless $node;
    return defined $self->{node_map}{ $node->id };
}

sub get_node {
    my ( $self, $node ) = @_;
    return unless $node;
    return $self->{node_map}{ $node->id };
}

sub delete_node {
    my ( $self, $node ) = @_;
    return unless $node;
    return delete $self->{node_map}{$node->id} if $node;
}

for my $name (qw( has_node get_node delete_node )) {
    around $name => sub {
        my $orig = shift;
        my $self = shift;
        my $node = shift;

        croak "Cannot $name without a node or an identifier" unless $node;

        $node = $self->{node_map}{$node}
            unless blessed $node && $node->DOES('Graph::Base::Role::Node');

        $self->$orig($node);
    };
}

sub add_edge {
    my $self = shift;

    croak 'Cannot add edge between unspecified nodes' unless @_;

    my @nodes;
    if ( @_ == 1 && $_[0]->DOES('Graph::Base::Role::Edge') ) {
        my $edge = shift;

        croak 'Cannot add edge of an unrelated class'
            unless $edge->isa( $self->edge_class );

        my $from = $self->add_node( $edge->from );
        my $to = $from->link( $edge, @_ );

        $edge = $from->get_link($to);
        $to->link( $edge->reverse, @_ ) unless $self->directed;

        @nodes = ( $edge->from, $edge->to );
    }
    else {
        @nodes = ( shift, shift );
    }

    my ( $from, $to ) = map { $self->add_node($_) } @nodes;

    $from->link( $to, @_ );
    $to->link( $from, @_ ) unless $self->directed;

    return $from->get_link($to);
}

sub delete_edge {
    my $self = shift;

    croak 'Cannot delete unspecified edge' unless @_;

    my @nodes;
    if ( @_ == 1 && $_[0]->DOES('Graph::Base::Role::Edge') ) {
        @nodes = $_[0]->nodes;
    }
    else {
        @nodes = ( shift, shift );
    }

    my $from = $self->get_node( shift @nodes );
    my $to   = $self->get_node( shift @nodes );

    if ( $from && $to ) {
        $from->unlink( $to );
        $to->unlink( $from ) unless $self->directed;
    }

    return $self;
}

sub has_edge {
    my $self = shift;

    croak 'Cannot check existence of unspecified edge' unless @_;

    my $from = $self->get_node(shift) or return;
    my $to   = $self->get_node(shift) or return;

    return $from->has_link( $to );
}

sub get_edge {
    my $self = shift;

    croak 'Cannot fetch unspecified edge' unless @_;

    my $from = $self->get_node(shift) or return;
    my $to   = $self->get_node(shift) or return;

    return $from->get_link( $to );
}

sub complete {
    my ($self, %args) = @_;

    for my $this ($self->nodes) {
        for my $that ($self->nodes) {
            next if $this->id eq $that->id;
            $this->link($that)->link($this);
        }
    }

    return $self;
}

sub clone {
    my ($self) = @_;

    my $clone = $self->new(
        node_class => $self->node_class,
        edge_class => $self->edge_class,
    );

    for ( $self->edges ) {
        my ( $from, $to ) = $_->nodes;

        $from = $clone->add_node( $from );
        $to   = $clone->add_node( $to );

        $clone->add_edge( $_->clone( from => $from, to => $to ) );
    }

    return $clone;
}

### TEMP

sub dump {
    my ($self) = @_;
    for my $this ( sort { $a->id cmp $b->id } $self->nodes) {
        print STDERR $this->name, "\n";
        for my $that ($this->successors) {
            print STDERR '    -> ' . $that->name, "\n";
        }
    }
}

1;

__END__

=encoding UTF-8

=head1 NAME

Graph::Base - A base graph class

=head1 DESCRIPTION

L<Graph::Base> is a base graph class. The objective of this is to provide a
more modern and easily extensible general-purpose implemetation of graphs
than is provided by other distributions.

=head1 ATTRIBUTES

=head2 directed

True if this is a directed graph, false otherwise. Defaults to false.

=head2 node_class

=head2 edge_class

The names of the classes to use when instantiating node and edge objects.
These default to L<Graph::Base::Node> and L<Graph::Base::Edge> respectively.

=head1 METHODS

=head2 Query methods

=head3 nodes

Returns the list of nodes in this graph, in no particular order.

=head3 vertices

A synonym for B<nodes>.

=head3 edges

Returns the list of edges in this graph, in no particular order.

=head3 genus

Returns this graph's genus. This is equal to

    $edges - $nodes + 1

=head3 density

Returns this graph's density.

For directed graphs, this is equal to

    $edges / $nodes * ( $nodes - 1 )

For undirected graphs, this is equal to

    $edges / ( $nodes * ( $nodes - 1 ) / 2 )

=head2 Node CRUD methods

=head3 add_node

    $graph->add_node
    $graph->add_node( $node )
    $graph->add_node( $name )
    $graph->add_node( %args )

Adds a node to the graph.

If called with no arguments, a new empty node will be added.

If called with a single argument, this can be the name of a node, or a
reference to a node.

If it is the name of a node, a node with that name will be created.

If it is the reference to a node that does not exist in this graph, a node
with the same name will be created.

If it is a node already on this graph, this method does nothing.

If called with more than one argument, these will be passed as is to the
constructor of the B<node_class>.

It returns the added node, or false if there was no node to add.

=head3 has_node {

    $graph->has_node( $id   )
    $graph->has_node( $node )

Returns true if the specified node, specified by ID or as a reference,
exists in this graph. Returns false otherwise.

=head3 get_node {

    $graph->get_node( $id   )
    $graph->get_node( $node )

Fetches a node from the graph by ID, returns false if no node by that ID
exists in the graph.

If given a reference to a node, it will look for a node that matches that ID.

=head3 delete_node

    $graph->delete_node( $id   )
    $graph->delete_node( $node )

Deletes a node by ID, or by the ID of a node passed by reference.

Returns the deleted node, or undefined, if there was nothing to delete.

=head2 Edge CRUD methods

These methods control the creation, removal, and selection of edges in the
graph.

They work on edges between two nodes that are specified either by passing an
edge object (that "does" the L<Graph::Base::Role::Edge> role), or by passing
two elements that identify the nodes (their IDs, names, references, or any
combination thereof).

Calling any of these methods without any arguments is an error.

=head3 add_edge

    $graph->add_edge( $edge )
    $graph->add_edge( $node1, $node2 )

    $graph->add_edge( $edge, %args )
    $graph->add_edge( $node1, $node2, %args )

Adds an edge between the specified nodes. Returns the created edge, as an
instance of B<edge_class>.

Any additional parameters not used to specify the nodes will be passed as is
to the edge constructor.

=head3 delete_edge

    $graph->delete_edge( $edge )
    $graph->delete_edge( $node1, $node2 )

Removes the edge between the specified nodes. Returns the the graph object.

=head3 has_edge

    $graph->has_edge( $edge )
    $graph->has_edge( $node1, $node2 )

Returns true if there is an edge between the two specified nodes, or false
otherwise.

=head3 get_edge

    $graph->get_edge( $edge )
    $graph->get_edge( $node1, $node2 )

Fetches the edge between the specified nodes. Returns the edge, or false if
no edge was found.

=head3 complete

Turns this graph into a complete graph: it creates edges between all nodes.

=head2 dump

Used for debugging, generates a string representation of this object.

This method might disappear in the future. It is not for external use.

=head1 AUTHOR

=over 4

=item *

José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2019 by José Joaquín Atria.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
