package Graph::Base::Grid;

use Moo;

use Carp 'croak';
use Ref::Util 'is_arrayref';

extends 'Graph::Base';
with 'Graph::Base::Role::Grid';

our $VERSION = '0.001007';
use namespace::clean;

has '+edge_class' => ( default => 'Graph::Base::Edge::Weighted' );
has '+node_class' => ( default => 'Graph::Base::Node::Cell' );

around BUILDARGS => sub {
    my ( $orig, $self ) = ( shift, shift );
    my $args = $self->$orig(@_);

    croak 'Unbounded grids must be lazy'
        if ! @{ $args->{dimensions} // [] }
        && exists $args->{lazy}
        && ! $args->{lazy};

    return $args;
};

sub BUILD {
    my ($self, $args) = @_;

    croak 'Lazy directed grids are not supported yet'
        if $self->lazy && $self->directed;

    return unless $self->bounded;

    return if $self->lazy;

    # Turns [ 2, 1, 3 ] into qw( {0,1} {0} {0,1,2} )
    my @specs = map { '{' . join(',', 0 .. $_-1 ) . '}' }
        @{ $self->dimensions };

    $self->add_node( coord => $_ )
        for map { [ split ':' ] } glob join ':', @specs;


    return unless $args->{complete};

    $self->connect_to_neighbours($_) for $self->nodes;
}

sub connect_to_neighbours {
    my ($self, $cell ) = @_;

    my $i;
    for ( $self->neighbourhood( $cell->coord ) ) {
        my $to = $self->get_cell($_) or next;
        next if $cell->id eq $to->id;
        $cell->link($to);
        $i++;
    }

    return $i;
}

sub split_from_neighbours {
    my ($self, $cell ) = @_;

    my $i;
    for ( $self->neighbourhood( $cell->coord ) ) {
        my $to = $self->get_cell($_) or next;
        next if $cell->id eq $to->id;
        $cell->unlink($to);
        $i++;
    }

    return $i;
}

sub has_cell { shift->has_node( join ',', @{ +shift } ) }

sub get_if_cell { shift->get_node( join ',', @{ +shift } ) }

sub get_cell {
    my $self  = shift;
    my $coord = shift;;

    return $self->add_node( coord => $coord, @_ )
        if ! $self->locked && $self->in_bounds($coord);

    return $self->get_node( join ',', @{ $coord } );
}

sub add_cell {
    my $self = shift;
    croak 'Cannot add cells to a locked grid' if $self->locked;
    $self->add_node( coord => @_ );
}

sub bounded { @{ shift->dimensions } ? 1 : 0 }

sub in_bounds {
    my ($self, $coord ) = @_;

    croak 'Cannot tell if coordinate is in bounds without a valid coordinate'
        unless $coord && is_arrayref $coord;

    # If this grid is unbounded, all coordinates are in bounds
    return 1 unless $self->bounded;

    my @dims  = @{ $self->dimensions };
    my @comps = @{ $coord };

    croak 'Coordinate does not have the same dimensions as grid'
        unless @dims == @comps;

    for ( 0 .. $#dims ) {
        return unless $comps[$_] >= 0 && $comps[$_] < $dims[$_];
    }

    return 1;
}

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Grid - A base grid class

=head1 DESCRIPTION

An L<Graph::Base::Grid> object is a L<Graph::Base::Graph> that I<does> the
L<Graph::Base::Role::Grid> role.

By default, its cells are L<Graph::Base::Node::Cell> instances, and are
connected by instances of L<Graph::Base::Edge::Weighted>.

Since this is a base grid implementation, it does not define a method to find
a cell's neighbours. Those will have to be implented by its subclasses.

See the documentation of those modules for more details.

=head1 METHODS

=head2 has_cell

    my $bool = $grid->has_cell( $coord )

Checks whether this grid has a cell at the given coord.

=head2 get_cell

    my $maybe_node = $grid->get_cell( $coord )

Returns the node at the given coordinate if it exists, or undefined if there
is no such node.

If the grid is set to be lazily populated, and it is not locked, and the
coordinate is within the grid's bounds, it will instead add a node at the
given coordinate and return it.

Otherwise, it will return undefined.

=head2 get_if_cell

    my $maybe_node = $grid->get_if_cell($coord)

Returns the node at the given coordinate if it exists, or undefined if there
is no such node.

Unlike C<get_cell> above, this method will I<never> create a node that doesn't
already exist. In that sense, it is equivalent to doing

    $grid->lock;
    my $node = $grid->get_cell($coord);
    $grid->unlock;

or

    my $node = $grid->get_cell($coord) if $grid->has_cell($coord);

=head2 add_cell

    my $node = $grid->add_cell( $coord, @options )

Adds a node at the given coordinate. Any arguments passed after the first will
be passed as-is to the constructor of the C<node_class>.

Calling this method while the grid is locked is an error and will throw an
exception.

=head2 bounded

    my $boolean = $grid->bounded;

Returns true if this grid is bounded. When a grid is bounded, it can only
contain cells within those bounds (defined by the grid's dimensions).

=head2 in_bounds

    my $boolean = $grid->in_bounds($coord);

Takes the coordinates of a cell and returns true if this cell is within the
bounds of this particular grid.

If the grid is unbounded, this method immediately returns true, since all cells
are considered to be within bounds.
