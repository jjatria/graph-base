package Graph::Base::Layouter;

use Moo;

use Scalar::Util qw( refaddr );
use Graph::Base::Grid::Square;
use Graph::Base::Types qw( Graph );

our $VERSION = '0.001007';
use namespace::clean;

has graph => (
    is => 'ro',
    isa => Graph,
    required => 1,
    weak_ref => 1,
);

has traversal => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $graph = $_[0]->graph;

        require Graph::Base::Traversal::DepthFirst;
        Graph::Base::Traversal::DepthFirst->new(
            graph => $graph,
            next_root => sub {
                my ( undef, $seen ) = @_;

                my ($root) =
                    sort { $a->predecessors <=> $b->predecessors }
                    grep { ! $seen->{ $_->id } }
                    $graph->nodes;

                return $root;
            },
        );
    },
);

sub reset {
    my ($self) = @_;
    $self->clear_ranks;
    $self->traversal->reset;
    return $self;
}

sub layout {
    my ( $self, %args ) = @_;

    my $dist  = $args{distance} // 2;
    my $coord = $args{start}    // [ 0 , 0 ];

    # An empty unbounded square grid
    my $grid = Graph::Base::Grid::Square->new( bounded => 0 );

    my @chains
        = sort { $b->length <=> $a->length || $a->dump cmp $b->dump }
        values %{ $self->find_chains };

    my %placed;


    PATH: for my $path (@chains) {
        NODE: for my $node ($path->nodes) {
            if ( $placed{ $node->id } ) {
                $coord = $placed{ $node->id };
                next;
            };

            my @candidates = ( $coord );
            push @candidates, $grid->neighbourhood( $coord, $dist );

            LOCATION: for my $location ( @candidates ) {
                my $cell = $grid->add_cell($location) or next;

                if ( $cell->is_empty ) {
                    $cell->content( $node );
                    $coord = $placed{ $node->id } = $location;

                    next NODE;
                }
            }
        }
    }

    return $grid;
}

sub calculate_ranks {
    my ($self) = @_;

    my $traversal = $self->traversal;
    $traversal->reset;

    my %ranks;

    $traversal->on( new_path => sub {
        my $path = shift->path;
        $ranks{ $path->start->id } = $path->length + 1;
    });

    $traversal->on( step => sub {
        my $event = shift;
        $ranks{ $event->node->id } = $event->path->length + 1;
    });

    $traversal->traverse;

    return \%ranks;
}

sub find_chains {
    my ($self) = @_;

    my $traversal = $self->traversal;
    $traversal->reset;

    my ( %chains, %backtrack );

    $traversal->on( backtrack => sub {
        my $path = shift->path;

        return if $backtrack{ refaddr $path }++;

        my $clone = $path->clone;
        $chains{ refaddr $clone } = $clone;
    });

    $traversal->on( step => sub {
        my $path = shift->path;
        delete $backtrack{ refaddr $path };
    });

    $traversal->on( new_path => sub {
        my $event = shift;
        return unless $event->has_path;

        my $old = $event->path;
        $chains{ refaddr $old } = $old;
    });

    $traversal->on( done => sub {
        my $event = shift;
        return unless $event->has_path;

        my $path = $event->emitter->path;
        return if $chains{ refaddr $path };

        my $clone = $path->clone;
        $chains{ refaddr $clone } = $clone;
    });

    $traversal->traverse;

    return \%chains;
}

1;
