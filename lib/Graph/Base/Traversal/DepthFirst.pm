package Graph::Base::Traversal::DepthFirst;

use Moo;
extends 'Graph::Base::Traversal';

use Log::Any '$log';
use Class::Load qw( load_class ); # For loading the path class

our $VERSION = '0.001007';
use namespace::clean;

has '+enqueue' => (
    default => sub {
        # Put the nodes at the beginning of the queue
        sub { unshift @{ +shift }, @{ +shift } };
    },
);

has postordered => (
    is => 'ro',
    lazy => 1,
    clearer => 1,
    default => sub { [] },
);

has path       => ( is => 'rw', clearer => 1 );
has path_class => ( is => 'ro', default => 'Graph::Base::Path' );

after reset => sub {
    my $self = shift;
    $self->clear_postordered;
    $self->clear_path;
    $self->unsubscribe($_) for qw( new_path step backtrack );
};

after visit => sub {
    my ( $self, $node, $edge ) = @_;

    # If we are visiting an edge it means we are currently on a path
    if ($edge) {
        my $path = $self->path;
        while ($path->nodes) {
            last if $path->has_link($node);

            $log->tracef('Backtracking to connect to %s', $node->id);

            $self->emit( backtrack => ( path => $path ) );

            my $end = $path->backtrack;

            push @{ $self->postordered }, $end;
        }

        $path->add($node);
        $self->emit( step => ( node => $node, path => $path ) );
    }
    # If we are not visiting an edge, it means we are starting a new path
    else {
        my $old_path;

        # If we have an existing path, make sure to store its nodes
        # that may before we create a new one
        if ( $old_path = $self->path ) {
            push @{ $self->postordered }, reverse $old_path->nodes;
        }

        load_class $self->path_class;
        my $new_path = $self->path_class->new( start => $node );

        $self->path( $new_path );
        $self->emit(
            new_path => (
                $old_path ? ( path => $old_path ) : (),
            )
        );
    }

    $log->tracef('%s', $self->path->dump) if $self->path;
};

# Return the preordered list of nodes
sub preordered_nodes { shift->ordered_nodes(@_ ) };

# Return the postordered list of nodes
sub postordered_nodes { @{ shift->postordered } };

1;
