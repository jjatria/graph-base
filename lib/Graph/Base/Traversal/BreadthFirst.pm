package Graph::Base::Traversal::BreadthFirst;

use Moo;
extends 'Graph::Base::Traversal';

our $VERSION = '0.001007';
use namespace::clean;

has '+enqueue' => (
    default => sub {
        # Put the nodes at the end of the queue
        sub { push @{ +shift }, @{ +shift } };
    },
);

1;
