package Graph::Base::Node;

use Moo;
use Carp 'croak';
use Scalar::Util qw( blessed refaddr );
use Class::Load 'load_class';

with 'Graph::Base::Role::Node';
with 'Graph::Base::Role::Renderer::Node';

our $VERSION = '0.001007';
use namespace::clean;

has name  => ( is => 'ro' );
has graph => ( is => 'rw', weak_ref => 1 );
has label => ( is => 'rw', lazy => 1, default => sub { shift->name // '' } );
has stash => ( is => 'ro', lazy => 1, default => sub { {} }, clearer => 1 );

has edge_class => (
    is => 'ro',
    lazy => 1,
    clearer => 1,
    default => sub {
        my $graph = $_[0]->graph or return 'Graph::Base::Edge';
        return $graph->edge_class;
    },
);

sub id { $_[0]->name // refaddr $_[0] }

sub predecessors { map { $_->from } shift->incoming }

sub successors { map { $_->to } shift->outgoing }

sub outgoing { values %{ shift->{_outgoing_edges} //= {} } }

sub incoming {
    my $self = shift;
    my $graph = $self->graph or return;
    return map { $graph->get_edge( $_, $self ) }
        keys %{ $self->{_incoming_ids} };
}

sub has_incoming {
    return !! %{ shift->{_incoming_ids} //= {} };
}

sub has_outgoing {
    return !! %{ shift->{_outgoing_edges} //= {} };
}

sub link {
    my $self = shift;

    croak 'Cannot link to unspecified node' unless @_;

    my $graph = $self->graph or return;

    my ( $edge, $to );
    if ( blessed $_[0] && $_[0]->DOES('Graph::Base::Role::Edge') ) {
        # If we received an $edge (as part of a call to $graph->clone )
        # we clone it and set ourselves as one of the endpoints.
        # This "feature" will remain undocumented for now, since it's not
        # clear it will remain.
        $edge = shift;
        $to = $graph->add_node( $edge->to ) or return;
        $edge = $edge->clone( from => $self, to => $to, @_ );
    }
    else {
        $to = $graph->get_node(shift) or return;

        return $to if $self->{_outgoing_edges}{$to->id};

        load_class $self->edge_class;
        $edge = $self->edge_class->new(
            from => $self,
            to   => $to,
            @_,
        );
    }

    $self->{_outgoing_edges}{$to->id} = $edge;
    $to->{_incoming_ids}->{$self->id} = 1;

    return $to;
}

sub unlink {
    my ( $self, $to ) = @_;

    my $id = blessed $to && $to->can('id') ? $to->id : "$to";
    my $edge = $self->{_outgoing_edges}{$id} or return;

    $to = $edge->to;

    delete $self->{_outgoing_edges}{$to->id};
    delete $to->{_incoming_ids}{$self->id};

    return $to;
}

sub has_link {
    my ( $self, $to ) = @_;
    return defined $self->{_outgoing_edges}
        { blessed $to && $to->can('id') ? $to->id : "$to" };
}

sub get_link {
    my ( $self, $to ) = @_;
    return $self->{_outgoing_edges}
        { blessed $to && $to->can('id') ? $to->id : "$to" };
}

sub dump {
    return '[ ' . ( $_[0]->label // $_[0]->id ) . ' ]';
}

sub clone {
    my $self = shift;
    return $self->new(
        name       => $self->name,
        edge_class => $self->edge_class,
        exists $self->{label} ? ( label => $self->label ) : (),
        @_,
    );
}

1;

__END__

=encoding UTF-8

=head1 NAME

Graph::Base::Node - A base node for graphs

=head1 DESCRIPTION

L<Graph::Base::Node> is a base class for nodes in a graph. Instances of this
class will most often not be created manually, but through a graph object.

=head1 ATTRIBUTES

=head2 name

A user-defined unique identifier for a node. If set, this will be used
internally as the node ID. It has no default.

=head2 label

A user-defined non-unique identifier for a node. If set, this will be used
when printing an identifier for the node. It defaults to the value of B<name>
or the empty string.

=head2 graph

An optional weak reference to the graph this node belongs to (most often a
L<Graph::Base> object).

=head2 stash

A hash reference holding miscellaneous node attributes. This will not be used
internally, and is provided for the convenience of this class' users.

=head1 METHODS

=head2 id

Returns a unique internal identifier. If B<name> has been set, this is a
shortcut for that value. Otherwise, it will return an automatically generated
ID.

=head2 predecessors

Returns the list of node objects that are connected to this node.

=head2 successors

Returns the list of node objects that this node is connected to.

=head2 outgoing

Returns the list of edge objects originating from this node.

=head2 incoming

Returns the list of edge objects terminating at this node.

=head2 has_incoming

Returns true if this node has any incoming edges.

=head2 has_outgoing

Returns true if this node has any outgoing edges.

=head2 link

    $node->link($id)
    $node->link($node)

Accepts either the ID of a node or a reference to the node itself, and
establishes a link between this node and that one.

Returns the linked node if the link was created, or if the link already
existed. Returns false if the target node was not in the same graph, or if
the source node was itself not in a graph.

=head2 unlink

    $node->unlink($id)
    $node->unlink($node)

Accepts either the ID of a node or a reference to the node itself, and
deletes a link between this node and that one.

Returns the target node if a link was removed, or false if there was no link
to delete.

=head2 has_link

    $node->link($id)
    $node->link($node)

Returns true if this node is linked to the target node, specified by ID or
passed as a reference.

=head2 get_link

    $node->get_link($id)
    $node->get_link($node)

Returns the edge connecting this node to the target node, specified by ID or
passed as a reference, or false if no such link exists.

=head2 clone

Returns a clone of the object, with the same attributes. This will not include
the stash, the graph, or any of the node's connections.

=head2 dump

Used for debugging, generates a string representation of this object.

This method might disappear in the future. It is not for external use.

=head1 SEE ALSO

=over 4

=item * L<Graph::Base>

=item * L<Graph::Base::Role::Node>

=back

=head1 AUTHOR

=over 4

=item *

José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2019 by José Joaquín Atria.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
