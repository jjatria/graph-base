package Graph::Base::PathFinder;

use Moo;
use Carp 'croak';
use Graph::Base::Path;
use Graph::Base::Heap;
use Graph::Base::Traversal::BreadthFirst;
use Graph::Base::Types qw( Graph Node );

our $VERSION = '0.001007';
use namespace::clean;

has graph => ( is => 'ro', isa => Graph, required => 1 );

# See https://www.redblobgames.com/pathfinding/a-star/introduction.html
sub find_path {
    my ( $self, %args ) = @_;

    my $graph = $self->graph;

    croak 'Cannot find path without start point'
        unless $args{start};

    croak 'Cannot find path without end point'
        unless $args{end};

    $args{start} = $graph->get_node($args{start})
        or croak 'Start point not in graph';

    $args{end} = $graph->get_node($args{end})
        or croak 'End point not in graph';

    croak 'Start and end points are the same'
        if $args{start}->id eq $args{end}->id;

    # Calculates the shortest path from the start point
    # to _all_ other points in the graph.
    # If a target point is provided, it will exit early as soon
    # as that point is hit.
    # Providing a heuristic function reduces search space,
    # and turns it into A*.
    my $leads_to = $self->dijkstra(%args);

    my $start_id = $args{start}->id;

    # Return immediately if we never actually reached the end point
    return unless $leads_to->{ $args{end}->id };

    my ( $here, @nodes );
    $here = $args{end};
    while ( $here->id ne $start_id ) {
        push @nodes, $here;
        $here = $leads_to->{ $here->id } or last;
    }

    my $path = Graph::Base::Path->new( start => $args{start} );
    $path->add($_) for reverse @nodes;

    return $path;
}

sub dijkstra {
    my ( $self, %args ) = @_;

    croak 'Cannot run without a start node'
        unless Node->check($args{start});

    croak 'End node must be a node'
        if $args{end} && ! Node->check($args{end});

    my $graph_is_grid = $self->graph->DOES('Graph::Base::Role::Grid');
    my $graph_is_bounded;

    if ( $graph_is_grid ) {
        $graph_is_bounded = $self->graph->bounded;
        croak 'Cannot traverse an unbounded grid without an end point'
            unless $graph_is_bounded || $args{end};
    }

    my %leads_to;
    my %cost_so_far = ( $args{start}->id => 0 );
    my $max_cost = $args{max_cost};

    my $traversal = Graph::Base::Traversal::BreadthFirst->new(
        graph     => $self->graph,
        start     => $args{start},
        next_root => undef,    # We don't want a next root in this traversal
        new_queue => sub { Graph::Base::Heap->new },
        enqueue   => sub {
            my ( $queue, $neighbours ) = @_;

            for my $edge ( @{$neighbours} ) {
                my ( $current, $next ) = $edge->nodes;

                my $next_id = $next->id;

                my $cost = $cost_so_far{ $current->id }
                    + ( $edge->can('weight') ? $edge->weight : 1 );

                next if $max_cost && $cost > $max_cost;

                next if $cost_so_far{ $next_id }
                    && $cost >= $cost_so_far{ $next_id };

                $cost_so_far{ $next_id } = $cost;

                if ( $args{heuristic} ) {
                    $cost += $args{heuristic}->(
                        $edge, $leads_to{ $current->id }
                    );
                }

                $queue->add( [ $cost, $edge ] );
            }
        },
        dequeue => sub { shift->poll },
    );

    $traversal->on( visit => sub {
        my $event = shift;
        $leads_to{ $event->node->id }
            = $event->has_edge ? $event->edge->from : undef;
    });

    # Exit early as soon as we get to the end
    if ( $args{end} ) {
        $traversal->on( visit => sub {
            my $event = shift;
            return $event->stop if $event->node->id eq $args{end}->id;
        });
    }

    # If this graph is an unbounded grid, then ensure visited nodes
    # have links to their neighbours
    # FIXME Is this actually necessary / desirable?
    if ( $graph_is_grid && ! $graph_is_bounded ) {
        $traversal->on( visit => sub {
            my $event = shift;
            $event->emitter->graph->connect_to_neighbours( $event->node );
        });
    }

    $traversal->traverse;

    return \%leads_to;
}

1;

__END__

=encoding UTF-8

=head1 NAME

Graph::Base::PathFinder - A class to find paths in grids

=head1 DESCRIPTION

L<Graph::Base::PathFinder> will find paths connecting nodes in a
L<Graph::Base::Grid> object.

=head1 ATTRIBUTES

=head2 graph

The graph on which to find a path.

=head1 METHODS

=head2 find_path

    my $path = $scout->find_path( start => $start, end => $end, %args )

Finds a path in the graph connecting the start and end nodes. Returns a
L<Graph::Base::Path> object, or undefined if no path could be found.

The C<start> and C<end> parameters are mandatory. All other parameters
will be passed directly to the C<dijkstra> method (see below).

=head2 dijkstra

    my $leads_to = $scout->dijkstra( start => $start, %args )

Given a start node on a graph, it will find the shortest sequence of nodes
connecting it and its surroundings.

If given an C<end> node, the method will return as soon as that node is
reached. Otherwise the traversal will continue until it covers the entire
graph, or until reaching any new node would increase the cost to more
than C<max_cost>;

Returns a hash reference in which the keys are the IDs of the visited nodes
and the values are the L<Graph::Base::Node> objects that I<lead to> that
node with the lowest cost. This can be used to reconstruct the path to the
end point with lowest cost by traversing the pointers back to the start.

An optional C<heuristic> argument can be set to a subroutine reference. If
so, this sub will be called every time a new node is enqueued for traversal
and called with the following signature:

    $callback->( $current_edge, $previous_node )

in which C<$this_edge> will be a reference to the L<Graph::Base::Edge>
object that will be traversed in order to reach the next node, and
C<$previous_node> a reference to the L<Graph::Base::Node> object which
leads into the current node (with the lowest cost from the start).

The heuristic is expected to return an estimate of the distance to the
C<end> point. This will be added to the cost of traversing the current
edge, and by doing so, will transform this algorithm into A*.

Setting a C<heuristic> without an C<end> is not an error,
but finding a use case is left as an exercise to the reader.

=head1 SEE ALSO

=over 4

=item * L<Graph::Base>

=item * L<Graph::Base::Node>

=item * L<Graph::Base::Edge>

=item * L<Graph::Base::Traversal>

=back

=head1 AUTHOR

=over 4

=item *

José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2019 by José Joaquín Atria.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
