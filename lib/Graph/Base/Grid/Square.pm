package Graph::Base::Grid::Square;

use Moo;
use Carp 'croak';
use Scalar::Util 'blessed';
use Ref::Util 'is_arrayref';

extends 'Graph::Base::Grid';

our $VERSION = '0.001007';
use namespace::clean;

sub neighbourhood { goto \&von_neumann_neighbourhood }

# Overwrite accessor with constant
use constant directions => [
    [  0, -1 ],  # North
    [ -1,  0 ],  # West
    [  0,  1 ],  # South
    [  1,  0 ],  # East
];

around BUILDARGS => sub {
    my ( $orig, $self ) = ( shift, shift );
    my $args = $self->$orig(@_);
    croak 'Do not define custom directions on grid subclasses'
        if @{ $args->{directions} // [] };
    return $args;
};

# Returns the Von Neumann neighbouhood with the specified range
sub von_neumann_neighbourhood {
    my ( $class, $coord, $range, $opts ) = @_;

    croak 'Cannot calculate neighbourhood without valid coord'
        unless is_arrayref $coord && scalar @{$coord} >= 2 ;

    my ( $x, $y ) = @{$coord};
    $range //= 1;

    return $coord if $range < 1;

    my $xx = -$range;
    my $yy = 0;

    my @coords;

    do {
        push @coords, [ $x - $xx, $y + $yy ];
        push @coords, [ $x - $yy, $y - $xx ];
        push @coords, [ $x + $xx, $y - $yy ];
        push @coords, [ $x + $yy, $y + $xx ];

        # Filling
        if ( ! $opts->{perimeter_only} ) {
            for ( $xx+1 .. -$xx-1 ) {
                push @coords, [ $x + $_, $y +  $yy ];
                push @coords, [ $x + $_, $y + -$yy ] if $yy;
            }
        }

        ++$xx;
        ++$yy;
    } while ( $xx < 0 );

    return @coords;
}

# From http://members.chello.at/~easyfilter/bresenham.html
#
#   0 1 2 3 4 5 6 7 8 9
# 0
# 1  3rd      |      4th
# 2  qrt    o o o    qrt
# 3       o       o
# 4     o           o       center         = [5, 5]
# 5  -- o     x     o --    range / radius = 3
# 6     o           o
# 7       o       o
# 8  2nd    o o o    1st
# 9  qrt      |      qrt
#
sub euclidean_neighbourhood {
    my ( $class, $coord, $range, $opts ) = @_;

    croak 'Cannot calculate euclidean neighbourhood without valid coord'
        unless is_arrayref $coord && scalar @{$coord} >= 2 ;

    my ( $x, $y ) = @{$coord};
    $range //= 1;

    return $coord if $range < 1;

    my $xx = -$range;
    my $yy = 0;
    my $err = 2 - 2 * $range;

    my @coords;

    do {
        # First quarter: E - S
        push @coords, [ $x + -$xx, $y +  $yy ];

        # Second quarter: S - W
        push @coords, [ $x + -$yy, $y + -$xx ];

        # Third quarter: W - N
        push @coords, [ $x +  $xx, $y + -$yy ];

        # Fourth quarter: N - E
        push @coords, [ $x +  $yy, $y +  $xx ];

        # Filling
        if ( ! $opts->{perimeter_only} && $yy < $range ) {
            for ( $xx+1 .. -$xx-1 ) {
                push @coords, [ $x + $_, $y +  $yy ];
                push @coords, [ $x + $_, $y + -$yy ] if $yy;
            }
        }

        my $r = $err;

        $err += ++$yy * 2 + 1 if $r <= $yy;
        $err += ++$xx * 2 + 1 if $r >  $xx || $err > $yy;
    } while ( $xx < 0 );

    return @coords;
}

# Returns the Moore neighbouhood with the specified range
sub moore_neighbourhood {
    my ( $class, $coord, $range, $opts ) = @_;

    croak 'Cannot calculate neighbourhood without valid coord'
        unless is_arrayref $coord && scalar @{$coord} >= 2 ;

    my ( $x, $y ) = @{$coord};
    $range //= 1;

    return $coord if $range < 1;

    my $xx = -$range;
    my $yy =  $range;

    my @coords;

    do {
        push @coords, [ $x + $xx, $y + $yy ];
        push @coords, [ $x - $yy, $y + $xx ];
        push @coords, [ $x - $xx, $y - $yy ];
        push @coords, [ $x + $yy, $y - $xx ];

        # Filling
        if ( ! $opts->{perimeter_only} && $yy < $range ) {
            for ( $xx+1 .. -$xx-1 ) {
                push @coords, [ $x + $_, $y +  $yy ];
            }
        }

        --$yy;
    } while ( $yy > $xx );

    return @coords;
}

1;
