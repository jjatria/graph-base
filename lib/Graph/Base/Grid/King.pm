package Graph::Base::Grid::King;

use Moo;
use Carp 'croak';
use Ref::Util 'is_arrayref';

extends 'Graph::Base::Grid::Square';

our $VERSION = '0.001007';
use namespace::clean;

sub neighbourhood { shift->moore_neighbourhood(@_) }

# Overwrite accessor with constant
use constant directions => [
    [  0, -1 ],  # North
    [ -1, -1 ],  # North-West
    [ -1,  0 ],  # West
    [ -1,  1 ],  # South-West
    [  0,  1 ],  # South
    [  1,  1 ],  # South-East
    [  1,  0 ],  # East
    [  1, -1 ],  # North-East
];

1;
