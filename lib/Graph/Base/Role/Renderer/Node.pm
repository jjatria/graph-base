package Graph::Base::Role::Renderer::Node;

use Moo::Role;

our $VERSION = '0.001007';
use namespace::clean;

sub render {
    my ( $self, %args ) = @_;

    my $label = $args{label}  ? $args{label}
        : $self->can('label') ? $self->label
        : $self->can('name')  ? $self->name
        : '';

    my $width  = length($label) + ( $args{margin} // 1 ) * 2;
    my $height = ( $args{margin} // 1 ) * 2;

    my $buffer = '+' .  ( '-' x $width ) . '+' . "\n";
    $buffer .=  '| ' . $label . ' |' . "\n";
    $buffer .= '+' .  ( '-' x $width ) . '+' . "\n";

    return $buffer;
};

1;
