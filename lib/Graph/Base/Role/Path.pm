package Graph::Base::Role::Path;

use Moo::Role;
use Graph::Base::Types qw( Node );

our $VERSION = '0.001007';
use namespace::clean;

requires 'start';
requires 'end';
requires 'length';
requires 'nodes';
requires 'add';
requires 'has_node';
requires 'has_link';
requires 'step';
requires 'backtrack';

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Role::Edge - A base interface for classes representing graph edges

=head1 DESCRIPTION

This module defines a base interface for classes representing edges in a graph.

=head1 ATTRIBUTES

=head2 from

The node at the beginning of this edge.

=head2 to

The node at the end of this edge.

=head1 METHODS

The following methods should be defined by implementations of this interface,
but this is how they are expected to behave.

=head2 nodes

When called in list context, returns the list of nodes in this edge.
The I<from> node should be returned first.
