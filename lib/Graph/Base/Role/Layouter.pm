package Graph::Base::Role::Layouter;

use Moo::Role;

use Graph::Base::Types qw( Graph );

our $VERSION = '0.001007';
use namespace::clean;

has graph => ( is => 'ro', isa => Graph, required => 1, weak_ref => 1 );

# To be extended by classes that do this role
sub reset {}

requires 'layout';

1;
