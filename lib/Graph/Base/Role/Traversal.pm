package Graph::Base::Role::Traversal;

use Moo::Role;
with 'Beam::Emitter';

use Types::Standard qw( Maybe CodeRef );
use Graph::Base::Types qw( Graph Node );
use Graph::Base::Event;
use Log::Any '$log';

our $VERSION = '0.001007';
use namespace::clean;

has graph => ( is => 'ro', isa => Graph, required => 1 );
has start => ( is => 'ro', isa => Node, predicate => 1 );

has ordered => (
    is      => 'ro',
    lazy    => 1,
    clearer => 1,
    default => sub { [] },
);

has queue => (
    is      => 'ro',
    lazy    => 1,
    clearer => 1,
    default => sub { $_[0]->new_queue->() },
);

has tree => (
    is      => 'ro',
    clearer => 1,
);

has is_done => (
    is       => 'ro',
    init_arg => undef,
    default  => 0,
);

sub done     { shift->{is_done} = 1 };
sub not_done { shift->{is_done} = 0 };

# Callbacks
has enqueue => (
    is       => 'ro',
    isa      => CodeRef,
    required => 1,
);

has dequeue => (
    is      => 'ro',
    isa     => CodeRef,
    default => sub {
        # Read from the beginning of the queue
        sub { shift @{ +shift } };
    },
);

has new_queue => (
    is => 'ro',
    isa => CodeRef,
    default => sub { sub { [] } },
);

has next_root => (
    is      => 'ro',
    isa     => Maybe[ CodeRef ],
    default => sub {
        return sub {
            my ( $graph, $seen ) = @_;
            for my $root ( $graph->nodes ) {
                return $root unless $seen->{ $root->id };
            }
            return;
        };
    },
);

around emit => sub {
    my ( $orig, $self, $event ) = ( shift, shift, shift );
    return $self->$orig( $event => ( class => 'Graph::Base::Event', @_ ) );
};

sub has_next_root_callback { defined shift->{next_root} };

sub reset {
    my ($self) = @_;

    $self->unsubscribe($_) for qw( new_root visit done reset );

    $self->clear_tree;
    $self->not_done;

    $self->clear_ordered;
    $self->clear_queue;

    $self->emit('reset');

    return $self;
}

sub ordered_nodes { @{ shift->ordered } };

# Walk through the graph generating a traversal tree
sub traverse {
    my ($self) = @_;

    until ( $self->is_done ) {
        my ( $node, $edge ) = $self->next;
        last unless $node;
        next if $self->seen( $node );

        $log->tracef( 'Landed on a new node: %s', $node->id );
        $self->visit( $node, $edge );
    }

    $self->done;
    $self->emit('done');

    return $self;
}

sub visit {
    my ( $self, $node, $edge ) = @_;
    return unless $node;

    $log->tracef('Visiting %s', $node->id);

    if ( $edge ) {
        $self->tree->add_edge( $edge );
    }
    else {
        $self->tree->add_node( $node );
    }

    my $event = $self->emit(
        visit => (
            $node ? ( node => $node ) : (),
            $edge ? ( edge => $edge ) : (),
        ),
    );
    $self->done if $event->is_stopped;

    push @{ $self->ordered }, $node;

    # By default, the successors for this node are placed at the
    # beginning of the queue so they are the next nodes to visit
    # This can be changed by modifying the enqueue callback

    my @neighbours = $node->outgoing;
    $log->tracef('Enqueuing '
        . join( ', ', map { '[ ' . $_->dump . ' ]' } @neighbours )
    );

    $self->enqueue->( $self->queue, \@neighbours );

    return $self;
}

# Return the next node and edge to visit
sub next {
    my ($self) = @_;

    my ( $node, $edge );

    if ( $edge = $self->dequeue->( $self->queue ) ) {
        $log->tracef('Found a queued edge: %s', $edge->dump);
        $node = $edge->to;
    }
    # If we have a start node, and we haven't seen it yet, use it
    elsif ( $self->has_start && ! $self->seen( $self->start ) ) {
        $node = $self->start;
        $log->tracef('We have a start: %s', $node->id);
    }
    elsif ( $self->has_next_root_callback ) {
        $node = $self->next_root->(
            $self->graph,
            +{ map { $_->id => 1 } $self->tree->nodes },
        );

        $log->tracef('We have a new root: %s', $node->id) if $node;

        $self->emit(
            new_root => (
                $node ? ( node => $node ) : ()
            )
        );
    }

    return ( $node, $edge );
}

sub seen {
    my $tree = shift->tree or return;
    return $tree->has_node( shift );
}

1;
