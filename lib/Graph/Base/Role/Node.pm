package Graph::Base::Role::Node;

use Moo::Role;

our $VERSION = '0.001007';
use namespace::clean;

requires 'id';

requires 'successors';
requires 'predecessors';
requires 'outgoing';
requires 'incoming';

requires 'link';
requires 'unlink';
requires 'has_link';
requires 'get_link';

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Role::Node - A base interface for classes representing graph nodes

=head1 DESCRIPTION

This module defines a base interface for classes representing nodes in a graph.

=head1 ATTRIBUTES

=head2 id

A unique identifier for this node.

=head1 METHODS

The following methods should be defined by implementations of this interface,
but this is how they are expected to behave.

=head2 General

=head3 successors

When called in list context, returns the list of nodes this node connects to.

=head3 predecessors

When called in list context, returns the list of nodes that connect to this
node.

=head3 outgoing

When called in list context, returns the list of edges starting at this node.

=head3 incoming

When called in list context, returns the list of edges ending at this node.

=head2 Edges

=head3 link

Takes a node (or its ID) and creates a link from this node to the one received
as argument.

=head3 unlink

Takes a node (or its ID) and deletes a link from this node to the one received
as argument if one exists. If no link exists, this method should have no
effect.

=head3 has_link

Takes a node (or its ID) and returns a true value if this node is connected to
it, or false otherwise.

=head3 get_link

Takes a node (or its ID) and returns the edge connecting this node to it, or
undefined, if no such edge exists.
