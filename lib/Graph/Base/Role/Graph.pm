package Graph::Base::Role::Graph;

use Moo::Role;

our $VERSION = '0.001007';
use namespace::clean;

has directed => ( is => 'ro' );

requires 'nodes';

requires 'add_node';
requires 'has_node';
requires 'get_node';
requires 'delete_node';

requires 'add_edge';
requires 'has_edge';
requires 'get_edge';
requires 'delete_edge';

requires 'complete';

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Role::Graph - A base interface for graph classes

=head1 DESCRIPTION

This module defines a base interface for graph classes. For the purposes of
this documentation, graphs are collections of nodes connected by edges.

=head1 ATTRIBUTES

=head2 directed

A directed graph is one in which edges have a direction. That is: an edge
connecting node A and B does not imply the existnce of an edge connecting
node B with A.

A graph that is not directed is an I<undirected> graph.

=head1 METHODS

The following methods should be defined by implementations of this interface,
but this is how they are expected to behave.

=head2 General

=head3 nodes

When called in list context, returns the list of nodes in this graph.

=head3 complete

A complete graph is one in which each node is connected to all other nodes.
Calling this method should make sure this graph has this property.

=head2 Nodes

=head3 add_node

Adds a node to a graph.

=head3 has_node

Takes a node (or its ID) and checks whether the that node is contained in
this graph.

=head3 get_node

Takes a node (or more likely its ID) and returns the node by that ID in this
graph. If none exists, this method should return undef.

=head3 delete_node

Takes a node (or its ID) and deletes it from this graph if it exists. This
action should also result in the deletion of all edges from and to this node.

Deleting a node that does not exist in the graph should produce no action
(making such a call should not be an error).

=head2 Edges

=head3 add_edge

Takes two nodes (or their IDs) and adds an edge connecting them to this graph.
If the graph is directed, the edge should connect the first node to the second.
Otherwise, this method should make sure the edge that is created goes both ways.

=head3 has_edge

Takes two nodes (or their IDs), or an edge object, and checks whether it
exists in this graph.

=head3 get_edge

Takes two nodes (or their ID) and returns the edge that connects them in this
graph. If none exists, this method should return undef.

=head3 delete_edge

Takes two nodes (or their IDs), or an edge object, and deletes it from this
graph if it exists. This action should B<not> result in the deletion of the
nodes at either end of this edge.

Deleting an edge that does not exist in the graph should produce no action
(making such a call should not be an error).
