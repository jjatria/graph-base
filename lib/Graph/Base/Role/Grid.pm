package Graph::Base::Role::Grid;

our $VERSION = '0.001007';

use Moo::Role;

with 'Graph::Base::Role::Graph';

requires 'has_cell';
requires 'get_cell';
requires 'add_cell';
requires 'in_bounds';
requires 'bounded';

use namespace::clean;

has directions => ( is => 'rw', default => sub { [] } );

has dimensions => ( is => 'ro', default => sub { [] } );

has lazy => ( is => 'ro', default => sub { ! $_[0]->directed } );

has locked => ( is => 'rw', init_args => undef, default => 0 );

sub lock { shift->locked(1) }

sub unlock { shift->locked(0) }

sub neighbourhood {}

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Role::Grid - A base interface for grid classes

=head1 DESCRIPTION

This module defines a base interface for grid classes. For the purposes of this
documentation, a grid is represented as a graph in which its nodes are
connected according to some regular pattern.

Common examples of grids are the squares in a chess board (which would be a
10 x 10 bounded grid), or the cells in Conway's game of life (which would
probably be an unbounded grid). See below for details on how these could be
represented.

=head1 ATTRIBUTES

=head2 dimensions

Takes an array ref with the maximum number of cells on any given axis the grid
should contain. If a grid has dimensions, it will be considered "bounded" (see
below), and attempts to add cells outside these bounds will produce no effect.

A 10 x 10 grid (eg. a chess board) would be specified with C<[ 10, 10 ]>.

=head2 lazy

Takes a boolean value. Specifies whether this grid should be lazily populated
or not. If set to false, all cells in a bounded grid will be populated
immediately after object creation.

Attempting to create a non-lazy unbounded grid is an error.

Defaults to true.

=head1 METHODS

=head2 lock

=head2 unlock

    $grid->lock;    # Grid is locked
    $grid->unlock;  # Grid is unlocked

Lock or unlock the state of a grid. When a grid is locked, no cells will be
implicitly added (if it is lazy), and attempting to specifically add a cell
will result in an error.

=head2 neighbourhood

    my @coords = $grid->neighbourhood( $coord );

Takes the coordinates of a cell, and returns the coordinates of all cells that
are its direct neighbours. That is, the coordinates of all the cells that this
cell connects to.

By default, this method does nothing. Classes implementing this role must
define their own rules to define what cells a given cell connects to.
