package Graph::Base::Event;

use Moo;
use Graph::Base::Types qw( Path Node Edge );

extends 'Beam::Event';

our $VERSION = '0.001007';
use namespace::clean;

has edge => ( is => 'ro', isa => Edge, weak_ref => 1, lazy => 1, predicate => 1 );
has node => ( is => 'ro', isa => Node, weak_ref => 1, lazy => 1, predicate => 1 );
has path => ( is => 'ro', isa => Path, weak_ref => 1, lazy => 1, predicate => 1 );

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Event - A base class for events in Graph::Base

=head1 DESCRIPTION

Events fired by the classes in L<Graph::Base> will be instances of this
class.

This class inherits from L<Bean::Event>. See the documentation for that class
for more information.

=head1 METHODS

=head2 path

    $path = $event->path

Get the path associated with this event.

=head2 node

    $node = $event->node

Get the node associated with this event.

=head2 edge

    $edge = $event->edge

Get the edge associated with this event.

=head2 has_path

    $bool = $event->has_path

Check whether this event has an associated path.

=head2 has_node

    $bool = $event->has_node

Check whether this event has an associated node.

=head2 has_edge

    $bool = $event->has_edge

Check whether this event has an associated edge.
