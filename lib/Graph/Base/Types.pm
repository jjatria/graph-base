use strict;
use warnings;

package Graph::Base::Types;

use Type::Library
    -base,
    -declare => qw(
        Edge
        Graph
        Node
        Path
    );

use Type::Utils -all;

our $VERSION = '0.001007';
use namespace::clean;

role_type Edge, { role => 'Graph::Base::Role::Edge' };

role_type Graph, { role => 'Graph::Base::Role::Graph' };

role_type Node, { role => 'Graph::Base::Role::Node' };

role_type Path, { role => 'Graph::Base::Role::Path' };

1;
