package Graph::Base::Heap;

use strict;
use warnings;
use Carp 'croak';

use Ref::Util 'is_arrayref';

our $VERSION = '0.001007';
use namespace::clean;

sub new {
    my $class = shift;
    my $self  = @_ ? @_ > 1 ? { @_ } : shift : {};

    $self->{heap} //= [];
    $self->{comp} //= -1 if delete $self->{desc};
    $self->{comp} //= 1;

    croak 'Illegal value for comparator: must be 1 or -1'
        unless abs($self->{comp}) == 1;

    bless $self, $class;
}

# Add an element to the heap
# The element can be a single numeric value, in which case that will be
# used as the rank, or an array reference, in which case the first one
# will be used for the rank and the second for the value
sub add {
    # add one element to the heap
    my ( $self, @elems ) = @_;
    my $heap = $self->{heap};

    for my $elem (@elems) {
        $elem = [ $elem ] unless is_arrayref $elem;
        if ( ! @{$heap} || ( $elem->[0] <=> $heap->[0][0] ) != $self->{comp} ) {
            # Either the first element, or smaller than or equal to it
            unshift @{$heap}, $elem;
        }
        else {
            push @{$heap}, $elem;
            # We only need to sort if it is greater than the last element
            $self->_sort_up
                unless ( $elem->[0] <=> $heap->[-1][0] ) == $self->{comp};
        }
    }
    return $self;
}

# Called in scalar context returns the next value
# Called in list context returns all values in order
sub poll {
    my $self = shift;
    my $heap = $self->{heap};

    my @values;
    while (@{$heap}) {
        my $e = shift @{$heap};
        $self->_sort_down;
        push @values, @{$e} > 1 ? $e->[1] : $e->[0];
        return $values[0] unless wantarray;
    }

    return @values;
}

# Return the next item, but without removing it from the heap
sub peek {
    my $e = shift->{heap}[0];
    return unless $e;
    return @{$e} > 1 ? $e->[1] : $e->[0]
}

# Empty the heap discarding all elements
sub clear { $_[0]->{heap} = []; return $_[0] }

# Get the number of elements in the heap
sub count { scalar @{ shift->{heap} } }

sub _sort_up {
    my ($self) = @_;
    my $heap = $self->{heap};

    return unless @{$heap};

    # Index of bottom item
    my $i = $#{$self->{heap}} or return;
    my $p = ( $i - 1 ) / 2;
    my $parent = $heap->[$p];

    while ( $parent && ( $parent->[0] <=> $heap->[$i][0] ) == $self->{comp} ) {
        @{$heap}[ $p, $i ] = @{$heap}[ $i, $p ];

        $i = $p;
        $p = ( $i - 1 ) / 2;
        $parent = $heap->[$p];
    }

    return $self;
}

sub _sort_down {
    my ($self) = @_;
    my $heap = $self->{heap};

    return unless @{$heap};

    # Index of top item
    my $i = 0;
    my $c = 2 * $i + 1;
    my $child = $heap->[$c];

    while ($child) {
        my $other = $heap->[ $c + 1 ];
        if ($other && ( $other->[0] <=> $child->[0] ) == -$self->{comp} ) {
            $child = $other;
            $c += 1;
        }

        @{$heap}[ $c, $i ] = @{$heap}[ $i, $c ]
            if ( $child->[0] <=> $heap->[$i][0] ) == -$self->{comp};

        $i = $c;
        $c = 2 * $i + 1;
        $child = $heap->[$c];
    }

    return $self;
}

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Heap - A simple heap class

=head1 SYNOPSIS

    my $heap = Graph::Base::Heap->new->add(
        [ 3 => 'baz' ], [ 1 => 'foo' ], [ 2 => 'bar' ] );

    my $first = $heap->poll;  # $first == 'foo'
    my @rest  = $heap->poll;  # @rest  == qw( bar baz )

=head1 DESCRIPTION

L<Graph::Base::Heap> implements a basic heap object.

A heap is a special kind of list, which remains sorted regardless of the order
in which elements are added to it.

=head1 ATTRIBUTES

=head2 desc

By default, objects of this class sort their elements in ascending order.

This can be altered by setting this attribute toa  true value on construction.

Note that this information is not exposed by this class. It's up to the
programmer to remember the setting for each heap, if they choose to alter it.

=head1 METHODS

=head2 add

    $heap = $heap->add([ $order, $value ])
    $heap = $heap->add($number)

Adds a new element to the heap. Elements are array references.

The first item in each element will specify the element's order (so this
must be a numeric), while the second will be its value.

As a special case, if the element is a numeric scalar it will be used as both.

=head2 poll

    $value  = $heap->poll
    @values = $heap->poll

In scalar context, returns the value of the first element of the heap.

By default (with an ascending heap) this will be the element with the lowest
order. If the heap was constructed with the C<desc> attribute set to true,
this will be element with the I<highest> order.

In list context, this returns all the elements of the heap in order, clearing
the heap.

=head2 peek

    $value = $heap->peek

Returns the value of the first element of the heap, without removing it from
the heap.

=head2 clear

    $heap = $heap->clear

Clears the heap, discarding all elements. Returns the heap after clearing.
