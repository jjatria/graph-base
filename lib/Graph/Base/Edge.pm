package Graph::Base::Edge;

use Moo;

with 'Graph::Base::Role::Edge';

our $VERSION = '0.001007';
use namespace::clean;

sub clone {
    return shift->new(@_);
}

sub reverse {
    my $self = shift;
    return $self->clone( from => $self->to, to => $self->from, @_ );
}

sub dump {
    sprintf '%s -> %s', $_[0]->from->id, $_[0]->to->id;
}

1;
