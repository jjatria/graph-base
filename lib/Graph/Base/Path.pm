package Graph::Base::Path;

use Moo;

use Carp 'croak';
use Graph::Base::Types qw( Node );
use Scalar::Util 'blessed';

our $VERSION = '0.001007';
use namespace::clean;

has start => ( is => 'ro', required => 1, isa => Node );

with 'Graph::Base::Role::Path';

sub end {
    my ($self) = @_;
    return $self->{edges}[-1]->to if @{ $self->{edges} //= [] };
    return $self->{start};
};

# Return the length of the path in edges, or in the sum of their weights
# Takes optional node from where to start calculating the length
sub length {
    my ($self, $node) = @_;

    # If we are getting the full length return from cache immediately
    return $self->{_length} //= 0 unless $node;

    my $id = Node->check($node) ? $node->id : "$node";

    # Otherwise, traverse path and count from given node

    my $length = 0;
    my $found;

    for my $edge ( @{ $self->{edges} // [] } ) {
        $found = 1 if !$found && $edge->from->id eq $id;
        next unless $found;
        $length += blessed $edge && $edge->can('weight') ? $edge->weight : 1;
    }

    return $length;
};

# Add an edge to the path
sub add {
    my ($self, $node) = @_;

    my $edge = $self->end->get_link( $node )
        or croak 'There is no edge connecting '
        . $self->end->label . ' to ' . $node->label;

    push @{ $self->{edges} //= [] }, $edge;

    $self->{_length}++;

    return $self;
}

# Check whether we can connect to a given node
sub has_link { shift->end->has_link(shift) }

# Check whether we can contain the specified node
sub has_node {
    my ( $self, $node ) = @_;

    my $id = Node->check($node) ? $node->id : "$node";

    return 1 if $self->start->id eq $id;

    for ( @{ $self->{edges} // [] } ) {
        return 1 if $_->to->id eq $id;
    }

    return;
}

# Remove an edge from the end of the path
# Returns the node that is no longer connected to the path
sub backtrack {
    my ($self) = @_;

    croak 'Cannot backtrack with an empty path'
        unless @{ $self->{edges} // [] };

    my $end = $self->end;
    pop @{ $self->{edges} };

    $self->{_length}--;

    return $end;
}

# Shifts the start to the next node in the path
# Returns the node that is no longer connected to the path
sub step {
    my ($self) = @_;

    croak 'Cannot step with an empty path'
        unless @{ $self->{edges} //= [] };

    my $start = $self->start;
    my $edge = shift @{ $self->{edges} };
    $self->{start} = $edge->to;

    $self->{_length}--;

    return $start;
}

sub follow {
    my ( $self, $cb ) = @_;
    $cb->($_) for @{ $self->{edges} // [] };
    return $self;
}

# return all the nodes in the chain as a list, in order.
sub nodes {
    my $self = shift;
    my @nodes = $self->start;
    $self->follow(sub { push @nodes, shift->to });
    return @nodes;
}

# TEMP

sub dump {
    return join( ' -> ', map { $_->dump } shift->nodes );
}

sub clone {
    my ($self) = @_;

    my @nodes = $self->nodes;

    my $clone = $self->new( start => shift @nodes );
    $clone->add($_) for @nodes;

    return $clone;
}

1;

__END__

=encoding UTF-8

=head1 NAME

Graph::Base::Path - A base node for paths in a graph

=head1 DESCRIPTION

For the purposes of this documentation, a path is a collection of edges
belonging to a graph. All paths will have a length (which will be determined
by the length or weight of the individual edges), and a start and end nodes
(which might be the same in the case of a path with no length).

=head1 ATTRIBUTES

=head2 start

The start node for this path. This must be specified during construction.

=head1 METHODS

=head2 end

    $node = $path->end;

Return the end node in thsi path. This could be the same node as the start,
if the path has no length.

=head2 length

    $length  = $path->length;
    $partial = $path->length($node);

Returns the length of the path. This will be equal to the sum of the weights
of each individual edge in the path. This will be read from the C<weight>
attribute in the edge.

Edges that do not have this method will be treated as edges with the unit
weight.

This method will optionally accept a node (or its ID) from where to start
counting the length. If the node does not exist in the path, the value will
be 0.

=head2 add

    $path = $path->add($id);
    $path = $path->add($node);

Takes a node object (or its ID) and adds the edge connecting the current
end of the path to that node to the list of edges in the path. On success,
the node received as argument will be the new end of the path.

Calling this method with a node that is not reachable from the end of the
path (see C<has_link>) is an error.

=head2 has_node

    $bool = $path->has_node($id);
    $bool = $path->has_node($node);

Takes a node object (or its ID) and returns true if the identified node is
in the path. Otherwise, it returns false.

=head2 has_link

    $bool = $path->has_link($id);
    $bool = $path->has_link($node);

Takes a node object (or its ID) and returns true if the path can connect to
the identified node (that is, if the target node is reachable from the node
returned by calling C<end> on the path).

=head2 backtrack

    $end = $path->backtrack;

Removes the last edge from the path and returns the node that is no longer
connected to the path (the previous end of the path).

Calling backtrack on an empty path is an error.

=head2 step

    $start = $path->step;

Removes the first edge from the path and returns the node that is no longer
connected to the path (the previous start of the path).

Calling step on an empty path is an error.

=head2 nodes

    @nodes = $path->nodes;

Returns a list with all the nodes reached by the path, in order from start
to end.

=head1 SEE ALSO

=over 4

=item * L<Graph::Base>

=item * L<Graph::Base::Node>

=item * L<Graph::Base::PathFinder>

=back

=head1 AUTHOR

=over 4

=item *

José Joaquín Atria <jjatria@cpan.org>

=back

=head1 COPYRIGHT AND LICENSE

This software is copyright (c) 2019 by José Joaquín Atria.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.

=cut
