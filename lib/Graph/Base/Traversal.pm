package Graph::Base::Traversal;

use Moo;
with 'Graph::Base::Role::Traversal';

our $VERSION = '0.001007';
use namespace::clean;

has '+tree' => (
    lazy => 1,
    default => sub {
        require Graph::Base;
        return Graph::Base->new(
            $_[0]->graph->directed ? ( directed => 1 ) : (),
        );
    },
);

1;
