package Graph::Base::Edge::Weighted;

use Moo;

extends 'Graph::Base::Edge';

our $VERSION = '0.001007';
use namespace::clean;

has weight => ( is => 'ro', default => 1 );

around clone => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig( @_, weight => $self->weight );
};

1;
