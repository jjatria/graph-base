package Graph::Base::Node::Cell;

use Moo;
use Types::Standard 'Bool';

extends 'Graph::Base::Node';

our $VERSION = '0.001007';
use namespace::clean;

has coord => ( is => 'ro', required => 1 );

has '+name' => ( lazy => 1, default => sub { join ',', @{ $_[0]->coord } } );

has content => ( is => 'rw', predicate => 1 );

sub is_empty { ! shift->has_content }

has cost => ( is => 'rw', default => 1 );

has is_passable => (
    is => 'rw',
    default => 1,
    isa => Bool,
    init_arg => 'passable',
    trigger => sub {
        my ( $self, $val ) = @_;
        return unless defined $val;

        unless ($val) {
            # Make impassable
            $self->unlink( $_->to )   for $self->outgoing;
            $self->unlink( $_->from ) for $self->incoming;
        }
    },
);

sub make_passable   { shift->is_passable(1) }
sub make_impassable { shift->is_passable(0) }

# A cell's cost is undefined while it is impassable, but setting it
# to "passable" again reveals the old cost.
around cost => sub {
    my ( $orig, $self ) = ( shift, shift );

    unless (@_) {
        return unless $self->is_passable;
        return $self->$orig;
    }

    return $self->$orig(@_);
};

around clone => sub {
    my $orig = shift;
    my $self = shift;
    my @args = @_;

    push @args, (
        # We bypass the accessor to clone the cell's cost even if it is
        # currently set to impassable
        cost     => $self->{cost},
        coord    => $self->coord,
        passable => $self->is_passable,
    );
    push @args, ( content => $self->content ) if $self->has_content;

    return $self->$orig(@args);
};

1;

=encoding UTF-8

=head1 NAME

Graph::Base::Node::Cell - A base class for nodes in a grid

=head1 DESCRIPTION

If grids are graphs, cells are their nodes. L<Graph::Base::Node::Cell>
provides a base class for cells in a grid.

In particular, cells require a coordinate during construction, and will use
this to generate a name (which can then be used to identify them).

Coordinates are array references where each element is one component. Using
non-numeric components is not an error, but will likely not do what you want.

Objects of this class inherit from L<Graph::Base::Node>. Read the
documentation in that class for more details.

=head1 ATTRIBUTES

=head2 coord

    $coord = $cell->coord
    $coord = $cell->coord($new_coord)

Sets or gets this cell's coordinates.

=head2 cost

    $cost = $cell->cost
    $cost = $cell->cost($new_cost)

Sets or gets this cell's movement cost. If this cell is currently marked as
impassable (see below), this cell's movement cost will be hidden and calling
this method as a reader will return C<undef>, regardless of what value has
been set.

Use C<make_passable> to unhide this cell's cost.

=head1 METHODS

=head2 make_impassable

    $cell->make_impassable

Set this cell as impassable. Doing so will remove all incoming and outgoing
edges, and hide this cell movement cost.

=head2 make_passable

    $cell->make_passable

Set this cell as passable. Doing so will unhide this cell's movement cost,
but I<will not> restore any previously existing edges.

=head2 is_passable

    $bool = $cell->is_passable

Checks whether this cell is currently set as passable.
