#!/usr/bin/env perl

use warnings;
use strict;
use utf8;
use uni::perl;
use feature qw( say state );
use curry;

use List::Util 'shuffle';
use Graph::Base::Traversal::DepthFirst;

my ( $width, $height ) = @ARGV ? @ARGV : ( 20, 20 );

# Use a ::King grid rather than a ::Square one for diagonal edges
use Graph::Base::Grid::Square;
my $grid = Graph::Base::Grid::Square->new(
    dimensions => [ $width, $height ],
    lazy       => 0,            # Create all nodes on construction
    complete   => 0,            # Do not connect cells on consturction
);

# Pick a random start (but avoid edges)
my ($start) = $grid->get_cell([
    1 + int( rand( $grid->dimensions->[0] - 2 ) ),
    1 + int( rand( $grid->dimensions->[1] - 2 ) ),
]);

my $traversal =  Graph::Base::Traversal::DepthFirst->new(
    graph     => $grid,
    start     => $start,
    next_root => undef,    # We don't want a next root in this traversal
);

$traversal->on(
    visit => $grid->$curry::weak(sub {
        # $grid is a weakened refs in this scope
        my ( $grid, $event ) = @_;

        my $node = $event->node;
        my $edge = $event->edge;

        my @edges;
        for my $direction ( shuffle @{ $grid->directions } ) {
            my ( $x, $y )   = @{ $node->coord };
            my ( $dx, $dy ) = @{ $direction };

            my $next = $grid->get_cell([ $x + $dx, $y + $dy ])
                or next;

            my @checks;

            # Moving diagonally
            if ( $dx && $dy ) {
                @checks = (
                    [ $x + 0,        $y + $dy     ],
                    [ $x + 0,        $y + $dy * 2 ],
                    [ $x + $dx,      $y + 0       ],
                    [ $x + $dx * 2 , $y + 0       ],
                    [ $x + $dx * 2 , $y + $dx * 2 ],
                    [ $x + $dx * 2 , $y + $dx     ],
                    [ $x + $dx,      $y + $dx * 2 ],
                );
            }
            # Moving horizontally
            elsif ( $dx ) {
                @checks = (
                    [ $x + $dx,     $y + $dy - 1 ],
                    [ $x + $dx,     $y + $dy     ],
                    [ $x + $dx,     $y + $dy + 1 ],
                    [ $x + $dx * 2, $y + $dy - 1 ],
                    [ $x + $dx * 2, $y + $dy     ],
                    [ $x + $dx * 2, $y + $dy + 1 ],
                );
            }
            # Moving vertically
            else {
                @checks = (
                    [ $x + $dx - 1, $y + $dy     ],
                    [ $x + $dx,     $y + $dy     ],
                    [ $x + $dx + 1, $y + $dy     ],
                    [ $x + $dx - 1, $y + $dy * 2 ],
                    [ $x + $dx,     $y + $dy * 2 ],
                    [ $x + $dx + 1, $y + $dy * 2 ],
                );
            }

            my $good = 1;
            for (@checks) {
                my $check = $grid->get_cell($_);
                if ( !$check || $check->outgoing ) {
                    $good = 0;
                    last;
                }
            }

            next unless $good;

            push @edges, $grid->add_edge( $node, $next );
        }

        my $emitter = $event->emitter;
        $emitter->enqueue->( $emitter->queue, \@edges );
    }),
);

$traversal->traverse;

my ( $w, $h ) = @{ $grid->dimensions };
for my $y ( 0 .. $h - 1 ) {
    my @line;
    for my $x ( 0 .. $w - 1 ) {
        my $cell  = $grid->get_cell([ $x, $y ]) or next;

        my $north = $grid->get_cell([ $x, $y-1 ]);
        my $south = $grid->get_cell([ $x, $y+1 ]);
        my $west  = $grid->get_cell([ $x-1, $y ]);
        my $east  = $grid->get_cell([ $x+1, $y ]);

        my $block = '';
        if ( $cell->outgoing ) {
            $block = '  ';
        }
        else {
            my $nw = $north && $west && $north->has_link($west);
            my $sw = $south && $west && $south->has_link($west);

            if ( $nw && $sw ) {
                $block .= '⯇';
            }
            elsif ( $nw ) {
                $block .= '◢';
            }
            elsif ( $sw ) {
                $block .= '◥';
            }
            else {
                $block .= '█';
            }

            my $ne = $north && $east && $north->has_link($east);
            my $se = $south && $east && $south->has_link($east);

            if ( $ne && $se ) {
                $block .= '⯈';
            }
            elsif ( $ne ) {
                $block .= '◣';
            }
            elsif ( $se ) {
                $block .= '◤';
            }
            else {
                $block .= '█';
            }
        }

        push @line, $block;
    }
    print @line;
    print "\n";
}
