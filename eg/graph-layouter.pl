#!/usr/bin/env perl

use utf8;
use uni::perl;
use feature qw( say state );
use DDP;

use Graph;
use Graph::Layouter::Spring;
use Graph::Renderer::Imager;

my $graph = Graph->new;

$graph->add_edge('A', 'B');
$graph->add_edge('B', 'C');
$graph->add_edge('B', 'D');

my $layouted = Graph::Layouter::Spring::layout( $graph );

use Imager;
use Imager::Color;

my $image = Imager->new( xsize => 800, ysize => 600, channels => 4 );
$image->box(
    color => Imager::Color->new( 0xff, 0xff, 0xff ),
    xmin => 0,
    ymin => 0,
    xmax => 800,
    ymax => 600,
    filled => 1
);

Graph::Renderer::Imager::render( $graph, $image );

$image->write( file => 'graph.png' );


