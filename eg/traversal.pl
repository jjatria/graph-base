#!/usr/bin/env perl

use utf8;
use uni::perl;
use feature qw( say state );
use DDP;

use Graph::Base;
my $graph = Graph::Base->new;

$graph->add_edge( 'Papá' => 'Mamá' );
$graph->add_edge( 'Mamá' => 'Consuelo' );
$graph->add_edge( 'Mamá' => 'Magdalena' );
$graph->add_edge( 'Mamá' => 'Fernando' );
$graph->add_edge( 'Mamá' => 'Matías' );
$graph->add_edge( 'Mamá' => 'Maximiano' );
$graph->add_edge( 'Mamá' => 'José Tomás' );
$graph->add_edge( 'Mamá' => 'José Joaquín' );

$graph->add_edge( 'Magdalena' => 'Mono' );
$graph->add_edge( 'Mono'      => 'Vicente' );
$graph->add_edge( 'Mono'      => 'Amanda' );

$graph->add_edge( 'Fernando' => 'Ximena' );
$graph->add_edge( 'Ximena'   => 'Antonia' );
$graph->add_edge( 'Ximena'   => 'Florencia' );
$graph->add_edge( 'Ximena'   => 'Clemente' );

$graph->add_edge( 'Matías'  => 'Marce' );
$graph->add_edge( 'Marce' => 'Pablo' );
$graph->add_edge( 'Marce' => 'Francisco' );

$graph->add_edge( 'Maximiano'  => 'Anne-Laure' );
$graph->add_edge( 'Anne-Laure' => 'Ada' );
$graph->add_edge( 'Anne-Laure' => 'Alma' );
$graph->add_edge( 'Anne-Laure' => 'Stella' );

$graph->add_edge( 'Maximiano'  => 'Claudia' );
$graph->add_edge( 'Claudia' => 'Martín' );
$graph->add_edge( 'Claudia' => 'Ema' );

$graph->add_edge( 'José Joaquín' => 'Koto' );

$graph->add_edge( 'Coté' => 'Jose' );
$graph->add_edge( 'Jose' => 'Azucena' );
$graph->add_edge( 'Jose' => 'Elena' );

$graph->dump;

use Graph::Base::Traversal::BreadthFirst;
use Graph::Base::Traversal::DepthFirst;

{
    # Breadth first
    my $traversal = Graph::Base::Traversal::BreadthFirst->new(
        graph => $graph,
        start => $graph->get_node('Papá'),
        next_root => sub {
            my (undef, $seen) = @_;
            return $graph->get_node('Coté') unless $seen->{'Coté'};
            return;
        },
    );

    $traversal->traverse;

    p $traversal->tree;
    p $traversal->tree->dump;

    my @nodes = $traversal->ordered_nodes;
    p @nodes;
}

{
    # Depth first
    my $traversal = Graph::Base::Traversal::DepthFirst->new(
        graph => $graph,
        start => $graph->get_node('Papá'),
        next_root => sub {
            my (undef, $seen) = @_;
            return $graph->get_node('Coté') unless $seen->{'Coté'};
            return;
        },
    );

    my $longest;

    $traversal->on( new_root => sub {
        say 'New path!';
        my $path = shift->emitter->path;

        if ($longest) {
            $longest = $path->clone if $path->length > $longest->length
        }
        else {
            $longest //= $path;
        }
    } );

    $traversal->on( backtrack => sub {
        say 'Backtracking!';
        my $path = shift->emitter->path;

        $longest = $path->clone
            if !$longest || $path->length > $longest->length;
    } );

    $traversal->traverse;

    p $traversal->tree;
    p $traversal->tree->dump;

    {
        my @nodes = map { $_->id } $traversal->postordered_nodes;
        p @nodes;
    }
    {
        my @nodes = map { $_->id } $traversal->preordered_nodes;
        p @nodes;
    }

    say 'Longest';
    say $longest->dump;
}
