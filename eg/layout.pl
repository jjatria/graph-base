#!/usr/bin/env perl

use utf8;
use uni::perl;
use feature qw( say state );
use DDP;

use Graph::Base;
my $graph = Graph::Base->new;

# $graph->add_edge( 'Papá' => 'Mamá' );
# $graph->add_edge( 'Mamá' => 'Consuelo' );
# $graph->add_edge( 'Mamá' => 'Magdalena' );
# $graph->add_edge( 'Mamá' => 'Fernando' );
# $graph->add_edge( 'Mamá' => 'Matías' );
# $graph->add_edge( 'Mamá' => 'Maximiano' );
# $graph->add_edge( 'Mamá' => 'José Tomás' );
# $graph->add_edge( 'Mamá' => 'José Joaquín' );
#
# $graph->add_edge( 'Magdalena' => 'Mono' );
# $graph->add_edge( 'Mono'      => 'Vicente' );
# $graph->add_edge( 'Mono'      => 'Amanda' );
#
# $graph->add_edge( 'Fernando' => 'Ximena' );
# $graph->add_edge( 'Ximena'   => 'Antonia' );
# $graph->add_edge( 'Ximena'   => 'Florencia' );
# $graph->add_edge( 'Ximena'   => 'Clemente' );
#
# $graph->add_edge( 'Matías'  => 'Marce' );
# $graph->add_edge( 'Marce' => 'Pablo' );
# $graph->add_edge( 'Marce' => 'Francisco' );
#
# $graph->add_edge( 'Maximiano'  => 'Anne-Laure' );
# $graph->add_edge( 'Anne-Laure' => 'Ada' );
# $graph->add_edge( 'Anne-Laure' => 'Alma' );
# $graph->add_edge( 'Anne-Laure' => 'Stella' );
#
# $graph->add_edge( 'Maximiano'  => 'Claudia' );
# $graph->add_edge( 'Claudia' => 'Martín' );
# $graph->add_edge( 'Claudia' => 'Ema' );
#
# $graph->add_edge( 'José Joaquín' => 'Koto' );
#
# $graph->add_edge( 'José Tomás' => 'Tatiana' );
#
# $graph->add_edge( 'Coté' => 'Jose' );
# $graph->add_edge( 'Jose' => 'Azucena' );
# $graph->add_edge( 'Jose' => 'Elena' );

$graph->add_edge('A', 'B');
$graph->add_edge('B', 'C');
$graph->add_edge('C', 'D');
$graph->add_edge('D', 'E');
$graph->add_edge('E', 'B');

$graph->add_edge('B', 'F');
$graph->add_edge('F', 'G');

$graph->add_edge('C', 'H');
$graph->add_edge('H', 'I');

use Graph::Base::Layouter::Easy;

my $layouter = Graph::Base::Layouter::Easy->new( graph => $graph );

use DDP;
# p $layouter->ranks;

say $_->dump for
    sort { $a->length <=> $b->length || $a->dump cmp $b->dump }
    values %{ $layouter->find_chains };

p $layouter->layout;
