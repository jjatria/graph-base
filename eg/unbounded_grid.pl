#!/usr/bin/env perl

use warnings;
use strict;

use utf8;
use uni::perl;
use feature qw( say state );
use DDP;
use List::Util 'shuffle';

use Graph::Base::Grid::Square;
use Graph::Base::PathFinder;

# Pick a random start and end point
my $start_coord = [ int rand 10, int rand 10 ];
my $end_coord   = [ int rand 10, int rand 10 ];

say 'Without heuristic function (Dijkstra)';
{
    my $grid = Graph::Base::Grid::Square->new( bounded => 0 );
    my $start = $grid->add_cell( $start_coord );
    my $end   = $grid->add_cell( $end_coord );

    my $path = Graph::Base::PathFinder->new( grid => $grid )
        ->find_path( start => $start, end => $end );

    say $path->dump if $path;
}

say 'With heuristic function (A*)';
{
    my $grid = Graph::Base::Grid::Square->new( bounded => 0 );
    my $start = $grid->add_cell( $start_coord );
    my $end   = $grid->add_cell( $end_coord );

    my $path = Graph::Base::PathFinder->new( grid => $grid )->find_path(
        start => $start,
        end   => $end,
        heuristic => sub {
            my ( $a, $b ) = ( $end->coord, shift->to->coord );
            # Manhattan distance on a square grid
            return abs( $a->[0] - $b->[0] ) + abs( $a->[1] - $b->[1] );
        },
    );
    say $path->dump if $path;
}

say 'With heuristic function (A*) penalising turns';
{
    my $grid = Graph::Base::Grid::Square->new( bounded => 0 );
    my $start = $grid->add_cell( $start_coord );
    my $end   = $grid->add_cell( $end_coord );

    my $path = Graph::Base::PathFinder->new( grid => $grid )->find_path(
        start => $start,
        end   => $end,
        heuristic => sub {
            my ( $edge, $prev ) = @_;

            my ( $z, $c ) = ( $end->coord, $edge->to->coord );

            # Manhattan distance on an orthogonal square grid
            my $dist = abs( $z->[0] - $c->[0] ) + abs( $z->[1] - $c->[1] );

            if ($prev) {
                my $a = $prev->coord;
                my ( $dx, $dy ) = ( $c->[0] - $a->[0], $c->[1] - $a->[1] );

                # Make turns more expensive
                $dist += 20 if $dx || $dy;
            }

            $dist;
        },
    );
    say $path->dump if $path;
}
