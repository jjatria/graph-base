#!/usr/bin/env perl

use warnings;
use strict;

use utf8;
use feature qw( say state );
use DDP;
use List::Util 'shuffle';

use Graph::Base::Grid::King;
use Graph::Base::PathFinder;

my $map = parse(<<'GRID');
...................
......555..........
......5555.555.....
.....55555255555...
.....5555522555....
...####5555255.....
...#..#5555........
......#5555........
...#..#55.....####.
...####.......#....
..............#....
GRID

# Pick a random start and end point
my ( $start, $end );

my @tiles = shuffle $map->nodes;
# do { $start = shift @tiles } until $start->is_passable;
do { $end   = shift @tiles } until $end->is_passable;

$start = $map->get_cell([4, 6]);


my $scout = Graph::Base::PathFinder->new( grid => $map );
my $path;

say 'Without heuristic function (Dijkstra)';
$path = $scout->find_path( start => $start, end => $end );
say $path->dump if $path;

say 'With heuristic function (A*)';
$path = $scout->find_path(
    start => $start,
    end   => $end,
    heuristic => sub {
        my ( $a, $b ) = ( $end->coord, shift->to->coord );
        # Manhattan distance on a square grid
        return abs( $a->[0] - $b->[0] ) + abs( $a->[1] - $b->[1] );
    },
);
say $path->dump if $path;

sub parse {
    my ( $string ) = @_;

    my %cost;

    my ( $w, $y ) = ( 0, 0 );

    for my $line ( split /\n/, $string ) {
        my $x = 0;

        for my $char ( split //, $line ) {
            $cost{"$x,$y"} = undef if $char eq '#';
            $cost{"$x,$y"} = $1 if $char =~ /([0-9])/;
            $x++;
        }

        $w = $x if $x > $w;
        $y++;
    }

    my $graph = Graph::Base::Grid::King->new(
        dimensions => [ $w, $y ], lazy => 0, complete => 1,
    );

    for my $id ( keys %cost ) {
        my $node = $graph->get_node($id);

        if ( defined $cost{$id} ) {
            $node->cost( $cost{$id} );
            $_->{cost} = $node->cost for $node->incoming;
            next;
        }

        $node->make_impassable;
    }

    return $graph;
}
