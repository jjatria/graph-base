#!/usr/bin/env perl

use warnings;
use strict;

use utf8;
use uni::perl;
use feature qw( say state );
use DDP;
use List::Util 'shuffle';

use Graph::Base::Heap;

my $q = Graph::Base::Heap->new;

my @values = shuffle 0 .. 20;

$q->add($_) for @values;

my @elems = $q->poll;

p @elems;
