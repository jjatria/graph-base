# NAME

Graph::Base - A base graph class

# DESCRIPTION

[Graph::Base](https://metacpan.org/pod/Graph::Base) is a base graph class. The objective of this is to provide a
more modern and easily extensible general-purpose implemetation of graphs
than is provided by other distributions.

# ATTRIBUTES

## directed

True if this is a directed graph, false otherwise. Defaults to false.

## node\_class

## edge\_class

The names of the classes to use when instantiating node and edge objects.
These default to [Graph::Base::Node](https://metacpan.org/pod/Graph::Base::Node) and [Graph::Base::Edge](https://metacpan.org/pod/Graph::Base::Edge) respectively.

# METHODS

## Query methods

### nodes

Returns the list of nodes in this graph, in no particular order.

### vertices

A synonym for **nodes**.

### edges

Returns the list of edges in this graph, in no particular order.

### genus

Returns this graph's genus. This is equal to

    $edges - $nodes + 1

### density

Returns this graph's density.

For directed graphs, this is equal to

    $edges / $nodes * ( $nodes - 1 )

For undirected graphs, this is equal to

    $edges / ( $nodes * ( $nodes - 1 ) / 2 )

## Node CRUD methods

### add\_node

    $graph->add_node
    $graph->add_node( $node )
    $graph->add_node( $name )
    $graph->add_node( %args )

Adds a node to the graph.

If called with no arguments, a new empty node will be added.

If called with a single argument, this can be the name of a node, or a
reference to a node.

If it is the name of a node, a node with that name will be created.

If it is the reference to a node that does not exist in this graph, a node
with the same name will be created.

If it is a node already on this graph, this method does nothing.

If called with more than one argument, these will be passed as is to the
constructor of the **node\_class**.

It returns the added node, or false if there was no node to add.

### has\_node {

    $graph->has_node( $id   )
    $graph->has_node( $node )

Returns true if the specified node, specified by ID or as a reference,
exists in this graph. Returns false otherwise.

### get\_node {

    $graph->get_node( $id   )
    $graph->get_node( $node )

Fetches a node from the graph by ID, returns false if no node by that ID
exists in the graph.

If given a reference to a node, it will look for a node that matches that ID.

### delete\_node

    $graph->delete_node( $id   )
    $graph->delete_node( $node )

Deletes a node by ID, or by the ID of a node passed by reference.

Returns the deleted node, or undefined, if there was nothing to delete.

## Edge CRUD methods

These methods control the creation, removal, and selection of edges in the
graph.

They work on edges between two nodes that are specified either by passing an
edge object (that "does" the [Graph::Base::Role::Edge](https://metacpan.org/pod/Graph::Base::Role::Edge) role), or by passing
two elements that identify the nodes (their IDs, names, references, or any
combination thereof).

Calling any of these methods without any arguments is an error.

### add\_edge

    $graph->add_edge( $edge )
    $graph->add_edge( $node1, $node2 )

    $graph->add_edge( $edge, %args )
    $graph->add_edge( $node1, $node2, %args )

Adds an edge between the specified nodes. Returns the created edge, as an
instance of **edge\_class**.

Any additional parameters not used to specify the nodes will be passed as is
to the edge constructor.

### delete\_edge

    $graph->delete_edge( $edge )
    $graph->delete_edge( $node1, $node2 )

Removes the edge between the specified nodes. Returns the the graph object.

### has\_edge

    $graph->has_edge( $edge )
    $graph->has_edge( $node1, $node2 )

Returns true if there is an edge between the two specified nodes, or false
otherwise.

### get\_edge

    $graph->get_edge( $edge )
    $graph->get_edge( $node1, $node2 )

Fetches the edge between the specified nodes. Returns the edge, or false if
no edge was found.

### complete

Turns this graph into a complete graph: it creates edges between all nodes.

## dump

Used for debugging, generates a string representation of this object.

This method might disappear in the future. It is not for external use.

# AUTHOR

- José Joaquín Atria <jjatria@cpan.org>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2019 by José Joaquín Atria.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
