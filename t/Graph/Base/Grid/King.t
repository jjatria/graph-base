use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base::Grid::King;

tests 'Directions' => sub {
    is +Graph::Base::Grid::King->directions, bag {
        item [  1,  0 ];
        item [  1, -1 ];
        item [  1,  1 ];
        item [  0, -1 ];
        item [  0,  1 ];
        item [ -1, -1 ];
        item [ -1,  1 ];
        item [ -1,  0 ];
        end;
    };
};

describe 'Default neighbourhood' => sub {
    my $grid = Graph::Base::Grid::King->new( dimensions => [ 9, 9 ] );

    describe 'Full neighbourhoods' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood([ 5, 5 ]) ;

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 4, 4 ];  #  0
                item [ 4, 5 ];  #  1
                item [ 4, 6 ];  #  2
                item [ 5, 4 ];  #  3
                item [ 5, 5 ];  #  4         o o o
                item [ 5, 6 ];  #  5         o o o
                item [ 6, 4 ];  #  6         o o o
                item [ 6, 5 ];  #  7
                item [ 6, 6 ];  #  8
                end;            #  9
            };
        };

        it 'Can specify neighbourhood range' => sub {
            my @coords = $grid->neighbourhood([ 5, 5 ], 3) ;

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 2 ];  #  0
                item [ 2, 3 ];  #  1
                item [ 2, 4 ];  #  2     o o o o o o o
                item [ 2, 5 ];  #  3     o o o o o o o
                item [ 2, 6 ];  #  4     o o o o o o o
                item [ 2, 7 ];  #  5     o o o o o o o
                item [ 2, 8 ];  #  6     o o o o o o o
                item [ 3, 2 ];  #  7     o o o o o o o
                item [ 3, 3 ];  #  8     o o o o o o o
                item [ 3, 4 ];  #  9
                item [ 3, 5 ];
                item [ 3, 6 ];
                item [ 3, 7 ];
                item [ 3, 8 ];
                item [ 4, 2 ];
                item [ 4, 3 ];
                item [ 4, 4 ];
                item [ 4, 5 ];
                item [ 4, 6 ];
                item [ 4, 7 ];
                item [ 4, 8 ];
                item [ 5, 2 ];
                item [ 5, 3 ];
                item [ 5, 4 ];
                item [ 5, 5 ];
                item [ 5, 6 ];
                item [ 5, 7 ];
                item [ 5, 8 ];
                item [ 6, 2 ];
                item [ 6, 3 ];
                item [ 6, 4 ];
                item [ 6, 5 ];
                item [ 6, 6 ];
                item [ 6, 7 ];
                item [ 6, 8 ];
                item [ 7, 2 ];
                item [ 7, 3 ];
                item [ 7, 4 ];
                item [ 7, 5 ];
                item [ 7, 6 ];
                item [ 7, 7 ];
                item [ 7, 8 ];
                item [ 8, 2 ];
                item [ 8, 3 ];
                item [ 8, 4 ];
                item [ 8, 5 ];
                item [ 8, 6 ];
                item [ 8, 7 ];
                item [ 8, 8 ];
                end;
            };
        };
    };

    describe 'Perimeter only' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood(
                [ 5, 5 ], undef, { perimeter_only => 1 });

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 4, 4 ];  #  0
                item [ 4, 5 ];  #  1
                item [ 4, 6 ];  #  2
                item [ 5, 4 ];  #  3
                item [ 5, 6 ];  #  4         o o o
                item [ 6, 4 ];  #  5         o   o
                item [ 6, 5 ];  #  6         o o o
                item [ 6, 6 ];  #  7
                end;            #  8
            };                  #  9
        };

        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood(
                [ 5, 5 ], 3, { perimeter_only => 1 });

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 2 ];  #  0
                item [ 2, 3 ];  #  1
                item [ 2, 4 ];  #  2     o o o o o o o
                item [ 2, 5 ];  #  3     o           o
                item [ 2, 6 ];  #  4     o           o
                item [ 2, 7 ];  #  5     o           o
                item [ 2, 8 ];  #  6     o           o
                item [ 3, 2 ];  #  7     o           o
                item [ 3, 8 ];  #  8     o o o o o o o
                item [ 4, 2 ];  #  9
                item [ 4, 8 ];
                item [ 5, 2 ];
                item [ 5, 8 ];
                item [ 6, 2 ];
                item [ 6, 8 ];
                item [ 7, 2 ];
                item [ 7, 8 ];
                item [ 8, 2 ];
                item [ 8, 3 ];
                item [ 8, 4 ];
                item [ 8, 5 ];
                item [ 8, 6 ];
                item [ 8, 7 ];
                item [ 8, 8 ];
                end;
            };
        };
    };
};

tests 'Directions' => sub {
    like dies {
        Graph::Base::Grid::King->new(
            directions => [
                [  1, -2 ],
                [ -1, -2 ],
                [ -2, -1 ],
                [ -2,  1 ],
                [ -1,  2 ],
                [  1,  2 ],
                [  2,  1 ],
                [  2, -1 ],
            ]
        );
    }, qr/Do not define custom directions on grid subclasses/;
};

done_testing;
