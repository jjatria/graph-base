use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base::Grid::Square;

tests 'Directions' => sub {
    is +Graph::Base::Grid::Square->directions, bag {
        item [  1,  0 ];
        item [  0, -1 ];
        item [  0,  1 ];
        item [ -1,  0 ];
        end;
    };
};

describe 'Default neighbourhood' => sub {
    my $grid = Graph::Base::Grid::Square->new( dimensions => [ 9, 9 ] );

    describe 'Full neighbourhoods' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood([ 5, 5 ]) ;

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 4, 5 ];  #  0
                item [ 5, 4 ];  #  1
                item [ 5, 5 ];  #  2
                item [ 5, 6 ];  #  3
                item [ 6, 5 ];  #  4           o
                end;            #  5         o o o
            };                  #  6           o
        };                      #  7
                                #  8
                                #  9

        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood([ 5, 5 ], 3) ;

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 5 ];  #  0
                item [ 3, 4 ];  #  1
                item [ 3, 5 ];  #  2           o
                item [ 3, 6 ];  #  3         o o o
                item [ 4, 3 ];  #  4       o o o o o
                item [ 4, 4 ];  #  5     o o o o o o o
                item [ 4, 5 ];  #  6       o o o o o
                item [ 4, 6 ];  #  7         o o o
                item [ 4, 7 ];  #  8           o
                item [ 5, 2 ];  #  9
                item [ 5, 3 ];
                item [ 5, 4 ];
                item [ 5, 5 ];
                item [ 5, 6 ];
                item [ 5, 7 ];
                item [ 5, 8 ];
                item [ 6, 3 ];
                item [ 6, 4 ];
                item [ 6, 5 ];
                item [ 6, 6 ];
                item [ 6, 7 ];
                item [ 7, 4 ];
                item [ 7, 5 ];
                item [ 7, 6 ];
                item [ 8, 5 ];
                end;
            };
        };
    };

    describe 'Perimeter only' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood(
                [ 5, 5 ], undef, { perimeter_only => 1 });

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 4, 5 ];  #  0
                item [ 5, 4 ];  #  1
                item [ 5, 6 ];  #  2
                item [ 6, 5 ];  #  3
                end;            #  4           o
            };                  #  5         o   o
        };                      #  6           o
                                #  7
                                #  8
                                #  9

        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->neighbourhood(
                [ 5, 5 ], 3, { perimeter_only => 1 });

            is \@coords, bag {  #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 5 ];  #  0
                item [ 3, 4 ];  #  1
                item [ 3, 6 ];  #  2           o
                item [ 4, 3 ];  #  3         o   o
                item [ 4, 7 ];  #  4       o       o
                item [ 5, 2 ];  #  5     o           o
                item [ 5, 8 ];  #  6       o       o
                item [ 6, 3 ];  #  7         o   o
                item [ 6, 7 ];  #  8           o
                item [ 7, 4 ];  #  9
                item [ 7, 6 ];
                item [ 8, 5 ];
                end;
            };
        };
    };
};

describe 'Euclidean neighbourhood' => sub {
    my $grid = Graph::Base::Grid::Square->new( dimensions => [ 9, 9 ] );

    it 'Dies without a valid central coordinate' => sub {
        like dies { $grid->euclidean_neighbourhood },
            qr/without valid coord/, 'Undefined';

        like dies { $grid->euclidean_neighbourhood('foo') },
            qr/without valid coord/, 'Not an array ref';

        like dies { $grid->euclidean_neighbourhood([1]) },
            qr/without valid coord/, 'Array ref with missing dimensions';
    };

    describe 'Full neighbourhoods' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            is [ $grid->euclidean_neighbourhood([ 5, 5 ]) ], bag {
                item [ 4, 5 ];
                item [ 5, 4 ];
                item [ 5, 5 ];
                item [ 5, 6 ];
                item [ 6, 5 ];
                end;
            };
        };

        it 'Returns center if radius is less than 1' => sub {
            is [ $grid->euclidean_neighbourhood([ 5, 5 ], 0) ],
                array { item [ 5, 5 ]; end };
        };

        it 'Can specify neighbourhood radius' => sub {
            my @coords = $grid->euclidean_neighbourhood( [ 5, 5 ], 3 );

            is \@coords, bag { #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 4 ]; #  0
                item [ 2, 5 ]; #  1
                item [ 2, 6 ]; #  2         o o o
                item [ 3, 3 ]; #  3       o o o o o
                item [ 3, 4 ]; #  4     o o o o o o o
                item [ 3, 5 ]; #  5     o o o o o o o
                item [ 3, 6 ]; #  6     o o o o o o o
                item [ 3, 7 ]; #  7       o o o o o
                item [ 4, 2 ]; #  8         o o o
                item [ 4, 3 ]; #  9
                item [ 4, 4 ];
                item [ 4, 5 ];
                item [ 4, 6 ];
                item [ 4, 7 ];
                item [ 4, 8 ];
                item [ 5, 2 ];
                item [ 5, 3 ];
                item [ 5, 4 ];
                item [ 5, 5 ];
                item [ 5, 6 ];
                item [ 5, 7 ];
                item [ 5, 8 ];
                item [ 6, 2 ];
                item [ 6, 3 ];
                item [ 6, 4 ];
                item [ 6, 5 ];
                item [ 6, 6 ];
                item [ 6, 7 ];
                item [ 6, 8 ];
                item [ 7, 3 ];
                item [ 7, 4 ];
                item [ 7, 5 ];
                item [ 7, 6 ];
                item [ 7, 7 ];
                item [ 8, 4 ];
                item [ 8, 5 ];
                item [ 8, 6 ];
                end;
            };
        };
    };

    describe 'Perimeters only' => sub {
        it 'Defaults to neighbourhood with unit radius' => sub {
            my @coords = $grid->euclidean_neighbourhood(
                [ 5, 5 ], undef, { perimeter_only => 1 });

            is \@coords, bag {
                item [ 4, 5 ];
                item [ 5, 4 ];
                item [ 5, 6 ];
                item [ 6, 5 ];
                end;
            };
        };

        it 'Returns center if radius is less than 1' => sub {
            my @coords = $grid->euclidean_neighbourhood(
                [ 5, 5 ], 0, { perimeter_only => 1 });

            is \@coords, bag { item [ 5, 5 ]; end };
        };

        it 'Can specify neighbourhood radius' => sub {
            my @coords = $grid->euclidean_neighbourhood(
                [ 5, 5 ], 3, { perimeter_only => 1 } );

            is \@coords, bag { #    0 1 2 3 4 5 6 7 8 9
                item [ 2, 4 ]; #  0
                item [ 2, 5 ]; #  1
                item [ 2, 6 ]; #  2         o o o
                item [ 3, 3 ]; #  3       o       o
                item [ 3, 7 ]; #  4     o           o
                item [ 4, 2 ]; #  5     o           o
                item [ 4, 8 ]; #  6     o           o
                item [ 5, 2 ]; #  7       o       o
                item [ 5, 8 ]; #  8         o o o
                item [ 6, 2 ]; #  9
                item [ 6, 8 ];
                item [ 7, 3 ];
                item [ 7, 7 ];
                item [ 8, 4 ];
                item [ 8, 5 ];
                item [ 8, 6 ];
                end;
            };
        };
    };
};

tests 'Directions' => sub {
    like dies {
        Graph::Base::Grid::Square->new(
            directions => [
                [  1, -2 ],
                [ -1, -2 ],
                [ -2, -1 ],
                [ -2,  1 ],
                [ -1,  2 ],
                [  1,  2 ],
                [  2,  1 ],
                [  2, -1 ],
            ]
        );
    }, qr/Do not define custom directions on grid subclasses/;
};

done_testing;
