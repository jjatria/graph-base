use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base::Grid;

tests 'Constructing grids and lazy grids' => sub {
    is +Graph::Base::Grid->new, object {
        call dimensions => [];
        call bounded    => F;
        call_list nodes => [];
    };

    is +Graph::Base::Grid->new( dimensions => [ 2, 2 ], lazy => 0 ), object {
        call dimensions => [ 2, 2 ];
        call bounded    => T;
        call_list nodes => bag {
            item object { call coord => [ 0, 0 ] };
            item object { call coord => [ 0, 1 ] };
            item object { call coord => [ 1, 0 ] };
            item object { call coord => [ 1, 1 ] };
            end;
        };
    };

    is +Graph::Base::Grid->new( dimensions => [ 2, 2 ] ), object {
        call dimensions => [ 2, 2 ];
        call bounded    => T;
        call_list nodes => [];

        # Lazily adds cells at given coordinates
        # This adds no more cells since the base grid does not specify a
        # cell's neighbours
        call [ get_cell => [ 0, 0 ] ] => object {
            call coord => [ 0, 0 ];
        };

        call [ get_cell => [ 1, 1 ] ] => object {
            call coord => [ 1, 1 ];
        };

        call [ has_cell => [ 0, 1 ] ] => F;
        call [ has_cell => [ 1, 1 ] ] => T;

        # Gets cell if it exists
        call [ get_if_cell => [ 0, 1 ] ] => U;
        call [ get_if_cell => [ 1, 1 ] ] => object {
            call coord => [ 1, 1 ];
        };

        # Returns undef because they are out of bounds
        call [ get_cell => [ -1, 1 ] ] => U;
        call [ get_cell => [  2, 2 ] ] => U;

        call_list nodes => bag {
            item object { call coord => [ 0, 0 ] };
            item object { call coord => [ 1, 1 ] };
            end;
        };
    }, 'Populate a lazy grid';

    like dies { Graph::Base::Grid->new( directed => 1, lazy => 1 ) },
        qr/Lazy directed grids are not supported/,
        'Lazy directed grids not supported';

    like dies { Graph::Base::Grid->new( lazy => 0 ) },
        qr/Unbounded grids must be lazy/,
        'Lazy unbounded grids not supported';
};

tests 'Grid bounds' => sub {
    is +Graph::Base::Grid->new( dimensions => [ 2, 2 ] ), object {
        call [ in_bounds => [ -1, 0 ] ] => F;
        call [ in_bounds => [  0, 0 ] ] => T;
        call [ in_bounds => [  0, 1 ] ] => T;
        call [ in_bounds => [  1, 0 ] ] => T;
        call [ in_bounds => [  1, 1 ] ] => T;
        call [ in_bounds => [  1, 2 ] ] => F;

        # Does not implicitly add cells
        call_list nodes => [];
    }, 'Bounded grid';

    is +Graph::Base::Grid->new, object {
        call [ in_bounds => [ -1, 0 ] ] => T;
        call [ in_bounds => [  0, 0 ] ] => T;
        call [ in_bounds => [  0, 1 ] ] => T;
        call [ in_bounds => [  1, 0 ] ] => T;
        call [ in_bounds => [  1, 1 ] ] => T;
        call [ in_bounds => [  1, 2 ] ] => T;

        # Does not implicitly add cells
        call_list nodes => [];
    }, 'Unbounded grid';

    like dies { Graph::Base::Grid->new->in_bounds },
        qr/Cannot tell if coordinate is in bounds without a valid coordinate/,
        'Calling in_bounds with no parameters dies';

    like dies { Graph::Base::Grid->new->in_bounds( 0, 1 ) },
        qr/Cannot tell if coordinate is in bounds without a valid coordinate/,
        'Calling in_bounds without an array ref dies';

    like dies {
            my $grid = Graph::Base::Grid->new( dimensions => [ 2, 2, 2 ] );
            $grid->in_bounds([ 0, 1 ]);
        },
        qr/Coordinate does not have the same dimensions as grid/,
        'Calling in_bounds with unspecified dimensions dies';

    like dies {
            my $grid = Graph::Base::Grid->new( dimensions => [ 2, 2 ] );
            $grid->in_bounds([ 0, 1, 1 ]);
        },
        qr/Coordinate does not have the same dimensions as grid/,
        'Calling in_bounds with extra dimensions dies';
};

tests 'Locking a grid' => sub {
    my $grid = Graph::Base::Grid->new( dimensions => [ 2, 2 ] );

    is $grid->locked, F, 'Grid is not locked';

    is $grid->add_cell([ 0, 0 ]), object { call coord => [ 0, 0 ] },
        'Can add a cell';

    is $grid->get_cell([ 1, 0 ]), object { call coord => [ 1, 0 ] },
        'Implicitly adds a cell';

    $grid->lock;
    is $grid->locked, T, 'Grid is locked';

    like dies { $grid->add_cell([ 1, 1 ]) },
        qr/Cannot add cells to a locked grid/,
        'Cannot add a cell to a locked grid';

    is $grid->get_cell([ 0, 1 ]), U, 'Does not implicitly add a cell';

    $grid->unlock;
    is $grid->locked, F, 'Grid is not locked';

    is $grid->add_cell([ 1, 1 ]), object { call coord => [ 1, 1 ] },
        'Can add a cell once again';

    is $grid->get_cell([ 0, 1 ]), object {
        call coord => [ 0, 1 ];
    }, 'Implicitly adds a cell once again';
};

tests 'Custom directions' => sub {
    my $horse = Graph::Base::Grid->new(
        directions => [
            [  1, -2 ],
            [ -1, -2 ],
            [ -2, -1 ],
            [ -2,  1 ],
            [ -1,  2 ],
            [  1,  2 ],
            [  2,  1 ],
            [  2, -1 ],
        ]
    );

    is $horse, object {
        call directions => array { prop size => 8; etc };
    }, 'Can create custom grids by defining directions';
};

done_testing;
