use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base::Heap;

my $heap = Graph::Base::Heap->new;

is $heap, object { prop blessed => 'Graph::Base::Heap' };

is $heap->add(1), exact_ref($heap), 'Add returns self';

$heap->add(5);
$heap->add([ 3, 'foo' ]);
$heap->add(4);

is $heap->poll, 1,
    'Poll in scalar context returns first value';

is $heap->count, 3, 'Count gets the number of elements';

is [ $heap->poll ], [ 'foo', 4, 5 ],
    'Poll in list context returns all values (and they are sorted)';

is [ $heap->poll ], [],
    'Heap is empty after reading';

$heap->add(5)->add(2)->add(6);

is $heap->count, 3, 'Can add by chaining';

is $heap->peek, 2, 'Peek looks at next element in heap';

is $heap->count, 3, 'Peek does not remove element from heap';

is $heap->clear, exact_ref $heap, 'Clear returns the heap';

is $heap->count, 0, 'Heap is empty after clearing';

$heap->add( 5, 4, 1, 3, 2 );

is [ $heap->poll ], [ 1, 2, 3, 4, 5 ], 'Can add multiple elements at once';

is [ Graph::Base::Heap->new( desc => 1 )->add( 5, 4, 1, 3, 2 )->poll ],
    [ 5, 4, 3, 2, 1 ],
    'Elements can be sorted in descending order';

done_testing;
