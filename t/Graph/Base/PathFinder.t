use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base;
use Graph::Base::PathFinder;

my $graph = Graph::Base->new;
$graph->add_node($_) for qw( A B C E D F G H );

$graph->add_edge( 'A' => 'B' );
$graph->add_edge( 'A' => 'D' );
$graph->add_edge( 'A' => 'E' );
$graph->add_edge( 'B' => 'C' );
$graph->add_edge( 'C' => 'E' );
$graph->add_edge( 'D' => 'E' );
$graph->add_edge( 'E' => 'F' );
$graph->add_edge( 'E' => 'G' );

#
#   .- D -.   G
#  /       \ /
# A ------- E   H
#  \       / \
#   B --- C   F
#

it 'Takes a graph on construction' => sub {
    is +Graph::Base::PathFinder->new( graph => $graph ), object {
        prop blessed => 'Graph::Base::PathFinder';
        call graph => exact_ref $graph;
    };
};

describe 'Find path' => sub {
    my $scout = Graph::Base::PathFinder->new( graph => $graph );

    tests 'Finds path between connected nodes' => sub {
        is $scout->find_path( start => 'A', end => 'G' ), object {
            prop blessed => 'Graph::Base::Path';
            call start => object { call id => 'A' };
            call end   => object { call id => 'G' };
        };
    };

    tests 'Does not find path between disconnected nodes' => sub {
        is $scout->find_path( start => 'A', end => 'H' ), U;
    };

    tests 'Dies without start and end nodes' => sub {
        like dies { $scout->find_path( end => 'B' ) },
            qr/Cannot find path without start/,
            'No start';

        like dies { $scout->find_path( start => 'A' ) },
            qr/Cannot find path without end/,
            'No end';

        like dies { $scout->find_path( start => 'A', end => 'Z' ) },
            qr/End point not in graph/,
            'End point not in graph';

        like dies { $scout->find_path( start => 'Z', end => 'B' ) },
            qr/Start point not in graph/,
            'Start point not in graph';
    };
};

describe 'Dijkstra' => sub {
    my $scout = Graph::Base::PathFinder->new( graph => $graph );

    it 'Validastes start and end nodes' => sub {
        like dies { $scout->dijkstra },
            qr/Cannot run without a start node/,
            'No args';

        like dies { $scout->dijkstra( start => 'A' ) },
            qr/Cannot run without a start node/,
            'Start is not a node';

        like dies {
                $scout->dijkstra(
                    start => $graph->get_node('A'),
                    end => 'B',
                )
            },
            qr/End node must be a node/,
            'End is not a node';
    };

    tests 'Maps shortest paths' => sub {
        my $a = $graph->get_node('A');
        my $e = $graph->get_node('E');

        is $scout->dijkstra( start => $a ), hash {
            # Start node is undefined
            field A => U;

            field B => object { call id => 'A' };
            # There are multiple paths of the same length leading to this node
            field C => object { call id => match qr/[EB]/ };
            field D => object { call id => 'A' };
            field E => object { call id => 'A' };
            field F => object { call id => 'E' };
            field G => object { call id => 'E' };

            # Unreachable node not in map
            field H => DNE;
            end
        }, 'Explores all graph without end';

        is $scout->dijkstra( start => $a, end => $e ), hash {
            # Start node is undefined
            field A => U;
            field E => object { call id => 'A' };

            # If these nodes exist, they will never ahve been reached from E
            field B => in_set( DNE, object { call id => 'A' } );
            field C => in_set( DNE, object { call id => 'B' } );
            field D => in_set( DNE, object { call id => 'A' } );

            # These nodes can only be reached from E, so they never will
            field F => DNE;
            field G => DNE;

            # Unreachable node not in map
            field H => DNE;
            end
        }, 'Exits early with end';
    };

    tests 'Accepts a heuristic (converts to A*)' => sub {
        # Arbitrary distances to 'G'
        my %distance = (
            A => 5, B => 4, C => 2, D => 2, E => 1, F => 2, G => 0, H => 1,
        );

        my $map = $scout->dijkstra(
            start => $graph->get_node('A'),
            end   => $graph->get_node('G'),
            heuristic => sub {
                my ( $here, $next, $prev ) = ( shift->nodes, shift );
                return $distance{ $next->id };
            },
        );

        is $map, hash {
            field A => U;
            field E => object { call id => 'A' };
            field G => object { call id => 'E' };
            end
        };
    };
};

done_testing;
