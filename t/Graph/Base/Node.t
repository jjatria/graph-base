use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base;
use Graph::Base::Node;
use Scalar::Util 'refaddr';

describe 'Attributes' => sub {
    my ( %params, $check, $message );

    case 'No parameters' => sub {
        %params = ();
        $message = 'Default values';
        $check = object {
            call sub { $_[0]->id  == refaddr $_[0] } => T;
            call name  => U;
            call label => '';
            call graph => F;
            call edge_class => 'Graph::Base::Edge';
        };
    };

    case 'Edge class' => sub {
        $message = 'Inherits edge_class from graph';
        %params = ( graph => Graph::Base->new( edge_class => 'Foo' ) );
        $check = object {
            call graph => T;
            call edge_class => 'Foo';
        };
    };

    case 'With name' => sub {
        $message = 'ID and label default to name if provided';
        %params = ( name => 'Steve' );
        $check = object {
            call id    => 'Steve';
            call name  => 'Steve';
            call label => 'Steve';
        };
    };

    case 'With label' => sub {
        $message = 'Label does not affect ID or name';
        %params = ( label => 'Node' );
        $check = object {
            call sub { $_[0]->id  == refaddr $_[0] } => T;
            call name  => U;
            call label => 'Node';
        };
    };

    tests 'Results' => sub {
        my $node = Graph::Base::Node->new( %params );
        is $node, $check, $message // '';
        is $node->dump, sprintf( '[ %s ]', $node->label() // $node->id ),
            'Dumps label or ID';
    };
};

describe 'Stash' => sub {
    it 'Accepts a stash' => sub {
        my $stash = { foo => 'bar' };
        is +Graph::Base::Node->new( stash => $stash )->stash, exact_ref $stash;
    };

    it 'Stash is lazy' => sub {
        my $node = Graph::Base::Node->new;

        is $node->{stash}, U;
        is $node->stash,   {};
        is $node->{stash}, {};
    };
};

describe 'Linking' => sub {
    my ( $graph, $node, $good, $bad );

    before_case 'Reset' => sub {
        $graph = Graph::Base->new;
        $graph->add_node($_) for qw( A B );
        $node = $graph->get_node('A');
    };

    case 'With node name as input' => sub {
        $good = 'B';
        $bad  = 'C';
    };

    case 'With node reference as input' => sub {
        $good = $graph->get_node('B');
        $bad  = Graph::Base::Node->new( name => 'C' );
    };

    it 'Links to a new node' => sub {
        is $node->has_link( $good ), F, 'Has no link to node at start';

        is $node->has_incoming, F, 'Has no links at all at start';

        is [ $node->outgoing ], [], 'Lists no outgoing edges at start';

        is $node->has_outgoing, F, 'Has no outgoing edges at start';

        is [ $node->successors ], [], 'Has no successor nodes at start';

        is $node->link( $good ), exact_ref $graph->get_node('B'),
            'Linking to node returns linked node on success';

        is $node->link( $bad ), F,
            'Linking to node returns false on failure';

        is $node->has_link( $good ), T,
            'Has a link to good node';

        is $node->has_link( $bad ), F,
            'Does not have a link to bad node';

        is [ $node->outgoing ], array {
            item object {
                prop blessed => $node->edge_class;
                call to => object {
                    prop blessed => ref $node;
                    call id      => 'B';

                    # Returns nodes
                    call_list predecessors => array {
                        item object {
                            prop blessed => ref $node;
                            call id => $node->id;
                        };
                        end;
                    };

                    # Returns edges
                    call_list incoming => array {
                        item object {
                            prop blessed => $node->edge_class;
                            call from => object {
                                call id => $node->id;
                            };
                        };
                        end;
                    };

                    call has_incoming => T;
                };
            };
            end;
        }, 'Lists edge to new node';

        is $node->has_outgoing, T, 'Has outgoing edges';

        is [ $node->successors ], array {
            item object {
                prop blessed => ref $node;
                call id      => 'B';
                call_list predecessors => array {
                    item object {
                        call id => $node->id;
                    };
                    end;
                };
            };
            end;
        }, 'Lists new node as successor';

        is $node->unlink( $good ), exact_ref $graph->get_node($good),
            'Unlinking from node returns target node on success';

        is $node->unlink( $bad ), U,
            'Unlinking from node returns undef on failure';

        is $node->has_link( $good ), F, 'No longer has a link to linked node';

        is [ $node->outgoing ], [], 'Has no outgoing edges';

        is [ $node->successors ], [], 'Has no successor nodes';
    };
};

describe 'Clone' => sub {
    my ( %args );

    before_case 'Reset args' => sub { %args = () };

    case 'Anonymous node' => sub {
        delete $args{name};
    };

    case 'Named node' => sub {
        $args{name} = 'Name';
    };

    describe 'Labels' => sub {

        case 'Without label' => sub {
            delete $args{label};
        };

        case 'With label' => sub {
            $args{label} = 'Label';
        };

        tests 'Results' => sub {
            my $graph = Graph::Base->new;
            my $stash = { foo => 'bar' };

            my $node  = Graph::Base::Node->new(
                %args,
                graph => $graph,
                stash => $stash,
            );

            is $node->clone, object {
                prop blessed => ref $node;
                prop this    => not_in_set( exact_ref $node );

                # Name and label are copied
                call name    => $node->name;
                call label   => $node->label;

                # Neither graph nor stash are copied
                call stash   => {};
                call graph   => U;

                # Id will be the same if node is named
                if ( $args{name} ) {
                    call id => $node->id;
                }
                else {
                    call id => not_in_set( $node->id );
                }
            };
        };
    };
};

done_testing;
