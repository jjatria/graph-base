use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base;
use Graph::Base::Path;

my $graph = Graph::Base->new;
$graph->add_node($_) for qw( A B C E D F G H );

$graph->add_edge( 'A' => 'B' );
$graph->add_edge( 'A' => 'D' );
$graph->add_edge( 'A' => 'E' );
$graph->add_edge( 'B' => 'C' );
$graph->add_edge( 'C' => 'E' );
$graph->add_edge( 'D' => 'E' );
$graph->add_edge( 'E' => 'F' );
$graph->add_edge( 'E' => 'G' );

#
#   .- D -.   G
#  /       \ /
# A ------- E   H
#  \       / \
#   B --- C   F
#

it 'Requires a start' => sub {
    is +Graph::Base::Path->new( start => $graph->get_node('A') ), object {
        prop blessed => 'Graph::Base::Path';
        call start   => object { call id => 'A' };
        call end     => object { call id => 'A' };
        call length  => 0;
    }, 'Can construct with start';

    like dies { Graph::Base::Path->new },
        qr/missing required arguments/i,
        'Cannot construct without start';

    like dies { Graph::Base::Path->new( start => 'A' ) },
        qr/did not pass type constraint "node"/i,
        'Cannot construct with ID';
};

it 'Can step and backtrack' => sub {
    my $path = Graph::Base::Path->new( start => $graph->get_node('A') );

    is $path->add( $graph->get_node('B') ), object {
        prop blessed =>'Graph::Base::Path';
        call end     => object { call id => 'B' };
        call length  => 1;
    }, 'Adding a node returns the path';

    like dies { $path->add( $graph->get_node('E') ) },
        qr/there is no edge connecting B to E/i,
        'Cannot add unreachable node';

    is $path->has_link('E'), F,
        'Can check link with node ID';

    is $path->has_link( $graph->get_node('E') ), F,
        'Can check link with node';

    $path->add( $graph->get_node($_) ) for qw( C E G );

    is $path, object {
        call start  => object { call id => 'A' };
        call end    => object { call id => 'G' };
        call length => 4;
        call nodes  => 5;
        call_list nodes => array {
            item object { call id => 'A' };
            item object { call id => 'B' };
            item object { call id => 'C' };
            item object { call id => 'E' };
            item object { call id => 'G' };
            end;
        };
    }, 'Path is in good state';

    is $path->has_node('A'), T, 'Has node by ID';
    is $path->has_node( $graph->get_node('G') ), T, 'Has node with object';

    is $path->step, object {
        prop blessed => 'Graph::Base::Node';
        call id => 'A';
    }, 'Step returns the old start node';

    is $path->backtrack, object {
        prop blessed => 'Graph::Base::Node';
        call id => 'G';
    }, 'Backtrack returns the old end node';

    is $path->has_node( $graph->get_node('A') ), F, 'No longer has node';
    is $path->has_node('G'), F, 'No longer has node by ID';

    is $path, object {
        call start  => object { call id => 'B' };
        call end    => object { call id => 'E' };
        call length => 2;
        call nodes  => 3;
        call_list nodes => array {
            item object { call id => 'B' };
            item object { call id => 'C' };
            item object { call id => 'E' };
            end;
        };
    }, 'Path is in good state';
};

it 'Dies if stepping or backtracking while empty' => sub {
    my $path = Graph::Base::Path->new( start => $graph->get_node('A') );

    like dies { $path->step }, qr/cannot step with an empty path/i,
        'Cannot step on empty path';

    like dies { $path->backtrack }, qr/cannot backtrack with an empty path/i,
        'Cannot backtrack on empty path';

};

it 'Can calculate partial length' => sub {
    my $path = Graph::Base::Path->new( start => $graph->get_node('A') );
    $path->add( $graph->get_node($_) ) for qw( B C E G );

    is $path->length, 4, 'Gets total length';
    is $path->length('A'), 4, 'Gets total length from start with ID';
    is $path->length( $graph->get_node('C') ), 2,
        'Gets partial length with node';
    is $path->length( $graph->get_node('H') ), 0,
        'Partial length with missing node returns 0';
};

it 'Can clone itself' => sub {
    my $path = Graph::Base::Path->new( start => $graph->get_node('A') );
    $path->add( $graph->get_node($_) ) for qw( B C E G );

    my $clone = $path->clone;

    is $clone, object {
        prop blessed    => ref $path;
        prop this       => not_in_set( exact_ref $path );
        call_list nodes => array {
            item exact_ref $_ for $path->nodes;
            end;
        }
    };
};

done_testing;
