use Test2::V0;
use Test2::Tools::Spec;

use Graph::Base;
use Graph::Base::Node;
use Graph::Base::Edge;
use Graph::Base::Edge::Weighted;
use Scalar::Util 'refaddr';

describe 'Attributes' => sub {
    it 'Defaults to undirected' => sub {
        is +Graph::Base->new->directed, F;
        is +Graph::Base->new( directed => 1 )->directed, T;
        dies { Graph::Base->new->directed(1) }, 'directed is read-only';
    };

    it 'Has an edge class' => sub {
        is +Graph::Base->new->edge_class, 'Graph::Base::Edge';
        is +Graph::Base->new( edge_class => 'Foo' )->edge_class, 'Foo';
        dies { Graph::Base->new->edge_class('Bar') }, 'edge_class is read-only';
    };

    it 'Has a node class' => sub {
        is +Graph::Base->new->node_class, 'Graph::Base::Node';
        is +Graph::Base->new( node_class => 'Foo' )->node_class, 'Foo';
        dies { Graph::Base->new->node_class('Bar') }, 'node_class is read-only';
    };
};

describe 'Node CRUD methods' => sub {
    my ( @params, %extra );

    case 'Anonymous node' => sub {
        @params = ();
        %extra = ( name => U, id => T );
    };

    case 'Named node' => sub {
        @params = ( 'A' );
        %extra = ( name => 'A', id => 'A' );
    };

    case 'Node with attributes' => sub {
        @params = ( name => 'A', label => 'LABEL' );
        %extra = ( name => 'A', id => 'A', label => 'LABEL' );
    };

    case 'Node by reference' => sub {
        note 'Add node';
        @params = Graph::Base::Node->new( name => 'A' );
        %extra = ( id => 'A' );
    };

    case 'Node by reference from another graph' => sub {
        my $graph = Graph::Base->new;
        @params = ( $graph->add_node('A') );
        %extra = ( graph => not_in_set( exact_ref $graph ) );
    };

    tests 'Results' => sub {
        my $graph = Graph::Base->new;

        my $node = $graph->add_node(@params);

        is $node, object {
            prop blessed => $graph->node_class;

            if (%extra) {
                call $_ => $extra{$_} for keys %extra;
            }
        };

        is $graph->has_node( $node ), T,
            'Graph has node by ref';

        is $graph->has_node( $node->id ), T,
            'Graph has node by ID';

        is $graph->has_node( Graph::Base::Node->new ), F,
            'Graph does not have node by ref';

        is $graph->has_node( 'B' ), F,
            'Graph does not have node by ID';

        is $graph->get_node( $node ), exact_ref $node,
            'Graph gets node by ref';

        is $graph->get_node( $node->id ), exact_ref $node,
            'Graph gets node by ID';

        is $graph->get_node( 'B' ), U,
            'Graph does not get node by ID';

        is $graph->get_node( Graph::Base::Node->new ), U,
            'Graph does not get node by ref';

        is $graph->delete_node( $node ), exact_ref $node,
            'Graph deletes node by ref';

        $node = $graph->add_node($node);

        is $graph->delete_node( $node->id ), exact_ref $node,
            'Graph deletes node by ID';

        is $graph->delete_node( 'B' ), U,
            'Graph does not delete node by ID';

        is $graph->delete_node( Graph::Base::Node->new ), U,
            'Graph does not delete node by ref';
    };
};

describe 'Edge CRUD methods' => sub {
    my ( @nodes, @params, %extra );

    before_case 'Reset' => sub { %extra = () };

    case 'Named nodes' => sub {
        @params = ( 'A', 'B' );
    };

    case 'Edge with attributes' => sub {
        @params = ( 'A', 'B', weight => 10 );
        %extra = ( weight => 10 );
    };

    case 'Nodes by reference' => sub {
        @params = (
            Graph::Base::Node->new( name => 'A' ),
            Graph::Base::Node->new( name => 'B' ),
        );
    };

    case 'Edge by reference' => sub {
        # Edges store weak references to nodes,
        # so we need to keep these in scope
        @nodes = (
            Graph::Base::Node->new( name => 'A' ),
            Graph::Base::Node->new( name => 'B' ),
        );

        @params = (
            Graph::Base::Edge::Weighted->new(
                from => $nodes[0], to => $nodes[1] )
        );
    };

    tests 'Results' => sub {
        my $graph = Graph::Base->new(
            edge_class => 'Graph::Base::Edge::Weighted',
        );

        is $graph->has_edge( 'A', 'B' ), F, 'Graph has no edge';
        is $graph->has_node('A'),        F, 'Graph does not have source node';
        is $graph->has_node('B'),        F, 'Graph does not have target node';

        my $edge = $graph->add_edge(@params);

        my $from = object { call id => 'A' };
        my $to   = object { call id => 'B' };

        is $edge, object {
            prop blessed => $graph->edge_class;
            call from    => $from;
            call to      => $to;
            call_list nodes   => array {
                item $from;
                item $to;
                end;
            };

           if (%extra) {
                call $_ => $extra{$_} for keys %extra;
            }
        }, 'add_edge returns edge';

        is $graph->has_edge( 'A', 'B' ), T, 'Graph has edge by ndoe names';
        is $graph->has_node('A'),        T, 'Adding edge added source node';
        is $graph->has_node('B'),        T, 'Adding edge added target node';

        is $graph->delete_edge( 'A', 'B' ), exact_ref $graph,
            'delete_edge with names returns graph';

        is $graph->has_edge( 'A', 'B' ), F, 'Graph no longer has edge';
        is $graph->has_node('A'),        T, 'Deleting edge kept nodes';
        is $graph->has_node('B'),        T, 'Deleting edge kept nodes';

        $edge = $graph->add_edge($edge);

        is $graph->delete_edge($edge), exact_ref $graph,
            'delete_edge with edge returns graph';

        is $graph->has_edge( 'A', 'B' ), F, 'Graph has edge by edge ref';
    };
};

it 'Lists nodes' => sub {
    my $graph = Graph::Base->new;

    is [ $graph->nodes ],    [], 'List of nodes starts empty';
    is [ $graph->vertices ], [], 'Can use vertices as synonym';

    # Adding same node more than once does nothing
    $graph->add_node($_) for qw( A B B C );

    my $check = array {
        all_items object {
            prop blessed => $graph->node_class;
            call id      => match qr/^[ABC]$/;
        };

        prop size => 3;
        etc;
    };

    is [ $graph->nodes ],    $check, 'Lists created nodes';
    is [ $graph->vertices ], $check, 'Lists created vertices';
};

it 'Lists edges' => sub {
    my $graph = Graph::Base->new;

    is [ $graph->edges ], [], 'List of edges starts empty';

    # Adding same node more than once does nothing
    $graph->add_node($_) for qw( A B B C );

    is $graph->complete, exact_ref $graph, 'Graph->complete returns graph';

    is [ $graph->edges ], array {
        all_items object {
            prop blessed  => $graph->edge_class;
        };

        prop size => $graph->nodes * 2;

        etc;
    };
};

# NOTE If this is not the genus, then what is this?
describe 'Calculates graph genus' => sub {
    my $graph;

    case 'Directed graph' => sub {
        $graph = Graph::Base->new( directed => 1 );
    };

    case 'Undirected graph' => sub {
        $graph = Graph::Base->new( directed => 0 );
    };

    tests 'Results' => sub {
        is $graph->genus, 1, 'Empty graph of genus 1';

        $graph->add_edge('A', 'B');
        $graph->add_edge('B', 'C');
        is $graph->genus, 0, 'Planar graph of genus 0';

        $graph->add_edge('C', 'A');
        is $graph->genus, 1, 'Thoroidal graph of genus 1';

        $graph->add_edge('B', 'D');
        $graph->add_edge('D', 'C');
        is $graph->genus, 2, 'Doubly-thoroidal graph of genus 2';
    };
};

# NOTE If this is not the density, then what is this?
describe 'Calculates graph density' => sub {
    tests 'Directed graph' => sub {
        my $graph = Graph::Base->new( directed => 1 );
        is $graph->density, 0, 'Empty graph of density 0';

        $graph->add_edge('A', 'B');
        $graph->add_edge('B', 'C');
        is $graph->density, within( 2 / 6 ), '3 vertices, 2 edges =~ 2/3';

        $graph->add_edge('C', 'A');
        is $graph->density, within( 3 / 6 ), '3 vertices, 3 edges =~ 3/6';

        $graph->add_edge('B', 'D');
        $graph->add_edge('D', 'C');
        is $graph->density, within( 5 / 12 ), '4 vertices, 5 edges =~ 5/12';

        $graph->complete;
        is $graph->density, 1, 'Complete graph of density 1';
    };

    tests 'Undirected graph' => sub {
        my $graph = Graph::Base->new( directed => 0 );
        is $graph->density, 0, 'Empty graph of density 0';

        $graph->add_edge('A', 'B');
        $graph->add_edge('B', 'C');
        is $graph->density, within( 2 / 3 ), '3 vertices, 2 edges =~ 2/3';

        $graph->add_edge('C', 'A');
        is $graph->density, within( 3 / 3 ), '3 vertices, 3 edges =~ 3/3';

        $graph->add_edge('B', 'D');
        $graph->add_edge('D', 'C');
        is $graph->density, within( 5 / 6 ), '4 vertices, 5 edges =~ 5/6';

        $graph->complete;
        is $graph->density, 1, 'Complete graph of density 1';
    };
};

package My::Test::Node {
    use Moo;
    extends 'Graph::Base::Node';
};

package My::Test::Edge {
    use Moo;
    extends 'Graph::Base::Edge';
};

# NOTE If this is not the density, then what is this?
describe 'Clone' => sub {
    tests 'Directed graph' => sub {
        my $graph = Graph::Base->new(
            directed   => 1,
            edge_class => 'My::Test::Edge',
            node_class => 'My::Test::Node',
        );

        $graph->add_node('A');
        $graph->add_node( name => 'B', label => 'Label cloned' );
        $graph->add_node(
            name  => 'C',
            label => 'Stash not cloned',
            stash => { foo => 'bar' },
        );

        $graph->complete;

        my $clone = $graph->clone;
        is $clone, object {
            prop blessed    => ref $graph;
            prop this       => not_in_set( exact_ref $graph );
            call_list nodes => bag {
                item object {
                    prop blessed => $graph->node_class;
                    call id => 'A';
                };

                item object {
                    prop blessed => $graph->node_class;
                    call id => 'B';
                    call label => 'Label cloned';
                };

                item object {
                    prop blessed => $graph->node_class;
                    call id => 'C';
                    call label => 'Stash not cloned';
                    call stash => {};
                };

                end;
            };
            call_list edges => bag {
                for ( $graph->edges ) {
                    my ( $from, $to ) = $_->nodes;
                    item object {
                        prop blessed => $graph->edge_class;
                        call from => object {
                            prop this => not_in_set( exact_ref $from );
                            call id   => $from->id;
                        };
                        call to => object {
                            prop this => not_in_set( exact_ref $to );
                            call id   => $to->id;
                        };
                    };
                }

                end;
            };
        };
    };
};

done_testing;
